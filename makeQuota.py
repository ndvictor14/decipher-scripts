#!/usr/bin/env hpython

__author__ = """
      (`-')  _               (`-')                   (`-')  
     _(OO ) (_)     _        ( OO).->       .->   <-.(OO )  
,--.(_/,-.\ ,-(`-') \-,-----./    '._  (`-')----. ,------,) 
\   \ / (_/ | ( OO)  |  .--./|'--...__)( OO).-.  '|   /`. ' 
 \   /   /  |  |  ) /_) (`-')`--.  .--'( _) | |  ||  |_.' | 
_ \     /_)(|  |_/  ||  |OO )   |  |    \|  |)|  ||  .   .' 
\-'\   /    |  |'->(_'  '--'\   |  |     '  '-'  '|  |\  \  
    `-'     `--'      `-----'   `--'      `-----' `--' '--' 
"""

import pkg_resources, hstub
from hermes import Survey, misc
from util import fatal
import argparse
import os
import sys
import subprocess
#import xlsxwriter
import xlwt
import re

# function to make friendly markers 
def safeString(theString):
    newString = re.sub(' |-','_',theString)
    return newString


# Specify script options
parser = argparse.ArgumentParser(description='Make a quota sheet from the command line.', add_help=False)
parser.add_argument('-l','--limits',help='Comma delimited list of sample quota limits.')
parser.add_argument('-v','--verbose',action="store_true", help="Verbose output.")
parser.add_argument('-h','--help',action="store_true", help="Open manpage.")
parser.add_argument('-q','--questions',nargs="+", help="Questions to add quota limits to. Space delimited. To specify limits/row: Q1:inf,inf,20 will set Q1r1 & Q1r2 limits to inf and Q1r3 limits to 20. If you want to set all row limits to inf only specify question label.")
args = parser.parse_args()

if args.help:
    subprocess.call(['man','/home/jaminb/v2/temp/victor/scripts/manpages/./makeQuota2'])
    exit()


pkg_resources,hstub

# Create the survey Object --- Since making a quota should be called within the appropriate directory to avoid incorrectly overwriting quotas for a different project, specified paths will not be allowed 
surveyName = "."

surveyName = misc.expandSurveyPath(surveyName)
theSurvey = Survey.loadNoException(surveyName)
if theSurvey == None:
    exit("Unable to open specified survey: %s" % surveyName)


# Check Survey States
if theSurvey.root.state.closed:
    exit("Survey state is closed. If you really want to make changes to this, set it to testing.")
elif theSurvey.root.state.live:
    exit("Survey is LIVE! I will not let you corrupt data. If you're in a temp set it to testing!")



# Grab samplesource information
myss = theSurvey.root.samplesources.children
allSS = []
ssInfo = {}
for eachSS in myss:
    ssMarkerLabel = safeString(eachSS.title)
    ssInfo = {'title': eachSS.title, 'lvalue': eachSS.list, 'limit': 'inf', 'markerlabel': ssMarkerLabel}
    allSS.append(ssInfo)

if args.verbose:
    print "Found these samplesources: "
    for eachSS in allSS:
        print " " + eachSS['title'] + " "

# Update limits to those provided
if args.limits:
    allLimits = args.limits.split(',')
    for ss in allSS:
        if ( len(allLimits) < len(allSS) ) and ( allSS.index(ss) == len(allLimits) ):
            print "%s's limit's defaulting to inf." % ss['title']
        else:
            ss['limit'] = allLimits[allSS.index(ss)]

else:
    print "Sample source limits will default to infinity."


# Handle question limits
qInfo = {}
allQs = []

if args.questions:
    theQuestions = args.questions

    # Go through the specified questions
    for eachQ in theQuestions:
        # Check if row limits specified
        if ":" in eachQ:
            limitsSpecified = True
            eachQLabel  = eachQ.split(':')[0]
            eachQLimits = eachQ.split(':')[1] 
        else:
            eachQLabel = eachQ
            limitsSpecified = False

        # Make sure question element exists in the survey
        qEl = next((x for x in theSurvey.root.qElements if x.label == eachQLabel), None)
        if qEl == None:
            exit("Question %s was not found. Please check your entry." % eachQLabel)
        else:
            # The question element is found, if limits specified ensure there are that many rows in the question
            if limitsSpecified:
                if len(eachQLimits.split(',')) != len(qEl.rows):
                    exit("You specified %s limits for %s but %s only has %s rows." % (len(eachQLimits.split(',')),qEl.label,qEl.label,len(qEl.rows)))
            
                # Apply limits as necessary 
                QLimits = eachQLimits.split(',')
                for x in range(0,len(QLimits)):
#                for eachLimit in eachQLimits.split(','):
                    qInfo = {'qlabel': eachQLabel, 'altLabel': qEl.rows[x].cdata, 'qlimits': str(QLimits[x]),'markerdefinition': eachQLabel+"."+qEl.rows[x].label, 'markerlabel': eachQLabel+"_"+qEl.rows[x].label, 'title': qEl.title}
                    allQs.append(qInfo)
            else:
                for eachRow in qEl.rows:
                    qInfo = {'qlabel': eachQLabel, 'altLabel': eachRow.cdata, 'qlimits': 'inf','markerdefinition': eachQLabel+"."+eachRow.label, 'markerlabel':eachQLabel+"_"+str(qEl.rows[qEl.rows.index(eachRow)].label), 'title': qEl.title}
                    allQs.append(qInfo)


# allQs now has all the information we need in regards to question quotas
# allSS has all we need in regards to samplesource quotas 
# Now we write the quota sheet

if os.path.isfile('/home/jaminb/v2/'+theSurvey.path+'/quota.xls'):
    print "quota.xls found in current directory!\n"
    if not re.match(r'y|yes|Yes|YES|Y',raw_input("Are you sure you want to continue and overwrite? [Y/N]")):
        exit("Aborted. Thanks for flying Vic.")

myDir = "/home/jaminb/v2/"+theSurvey.path

quotaWorkbook = xlwt.Workbook()
definesSheet = quotaWorkbook.add_sheet('Defines')
quotasSheet = quotaWorkbook.add_sheet('Quotas')

# Overalls Define
definesSheet.write(0, 0, "Total")
definesSheet.write(0, 1, "1")
definesSheet.write(0, 2, "Total Respondents")


#OVERALL QUOTA TRACKER
quotasSheet.write(0,0,"#=Overall")
quotasSheet.write(1,0,"Total")
quotasSheet.write(1,1,'inf')

if args.verbose:
    print "Created Overall table. Marker: Total, Limit: inf (this is default)"

# Sample sources
quotasSheet.write(3,0,"#=Samplesources")
allSSCount = len(allSS) + 1
for i in range(1, allSSCount): # Start at one to account for the "TOTAL"
    # Definitions
    definesSheet.write(i, 0, allSS[i-1]['markerlabel'])
    definesSheet.write(i, 1, "list=='"+allSS[i-1]['lvalue']+"'")
    definesSheet.write(i, 2, allSS[i-1]['title'])
    
    # Quotas
    quotasSheet.write(i+3, 0, allSS[i-1]['markerlabel'])                      
    quotasSheet.write(i+3, 1, allSS[i-1]['limit'])
    if args.verbose:
        print "Created marker %s for %s with title %s and set limit to %s." % (allSS[i-1]['markerlabel'],"list=='"+allSS[i-1]['lvalue']+"'",allSS[i-1]['title'],allSS[i-1]['limit'])


# Question Quotas
allQCount = len(allQs) + 1
counter = 0
for i in range(1,allQCount):
    # Defines
    definesSheet.write(i+allSSCount-1, 0,allQs[i-1]['markerlabel'])
    definesSheet.write(i+allSSCount-1, 1,allQs[i-1]['markerdefinition'])
    definesSheet.write(i+allSSCount-1, 2,allQs[i-1]['altLabel'])
    
    # Quotas
    # Check which markers belong together, if it does only add to current quota table else make new quota table
    # If the curren't dict's title doesn't match the previous they don't belong together
    if allQs[i-1]['title'] != allQs[i-2]['title']:
        if counter != 0:
            counter+=1   # Account for the space if it's a new table
        quotasSheet.write(i+allSSCount+counter+3, 0, "#="+allQs[i-1]['title'])  # Plus 3 to account for Overall table and the additional space
        quotasSheet.write(i+allSSCount+counter+4, 0, allQs[i-1]['markerlabel'])
        quotasSheet.write(i+allSSCount+counter+4, 1, allQs[i-1]['qlimits'])
        counter+=1
    else: # This limit is for the same question
        quotasSheet.write(i+allSSCount+counter+3, 0, allQs[i-1]['markerlabel'])
        quotasSheet.write(i+allSSCount+counter+3, 1, allQs[i-1]['qlimits'])
    if args.verbose:
        print "Created marker %s for %s with title %s and set limit to %s." % (allQs[i-1]['markerlabel'],allQs[i-1]['markerdefinition'],allQs[i-1]['altLabel'],allQs[i-1]['qlimits'])





try:
    quotaWorkbook.save(myDir+"/quota.xls")
except:
    exit("Error saving spreadsheet.")


print "quota.xls created! Thanks for flying Vic!"
