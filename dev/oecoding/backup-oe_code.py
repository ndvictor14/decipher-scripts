#!/usr/bin/python2.7

__author__="""
  _                             
 (_|   |_/o                     
   |   |      __ _|_  __   ,_   
   |   |  |  /    |  /  \_/  |  
    \_/   |_/\___/|_/\__/    |_/
                                
"""


import argparse
import os
from subprocess import Popen, PIPE, check_output
import xlsxwriter
import xlrd
import re
import HTMLParser
import string
from xml.sax.saxutils import escape
import cgi

html_parser = HTMLParser.HTMLParser()
html_escape_table = {
        '"': "&quot;",
        "'": "&apos;",
        "&": "&amp;",
    }


parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-f', help="OE Coding xls file.")
parser.add_argument('--verbose', '-v', action="store_true", help="Verbose output.")
parser.add_argument('--exclude', '-e', action="store_true", help="Exclude a sheet. Will give prompt.")
args = parser.parse_args()
global verbatimsKey
verbatimsKey = ''
outputFiles = []
createdFiles = []
excluded = ""
bad_chars = "'[](){}<>"



def checkExistance(fileName):
    """Check existance of specified file"""
    if os.path.isfile(fileName):
        print "File %s found..." % fileName
    else:
        exit("%s does not exist in your current directory...Good-bye!" % fileName)

def safeString(s):
    """Decoded and escaped"""
    d = decoded(s)
    try:
        #return escape(d, html_escape_table)
        return cgi.escape(d,True)
    except:
        return d

        
def decoded(s):
    encodings = ['utf-8','cp1252','latin-1','ascii']
    if isinstance(s,str):
        return s
    else:
        for eachE in encodings:
            try:
                return str(s).translate(string.maketrans("", "", ),bad_chars).decode(eachE)
            except:
                pass
                



def initiateOutputFile():
    """Create output file"""
    open('output.xml','w')        
    
        
def getWorksheets(workbook):
    """Read xls and get workbook titles in an array"""
    wb = xlrd.open_workbook(workbook)
    worksheets = wb.sheet_names()
    names = []
        
    for ws in worksheets:        
        names.append(str(ws))
        if args.verbose:
            print "Found Sheet: %s" % ws
    if args.exclude:
        for eachSheet in names:
            print str(names.index(eachSheet)) + "-" + eachSheet
        remove = raw_input("Enter numbers to exclude (comma delimited) :")
        for x in remove.split(','):
            del names[int(x)]

    return names

def cleanUp():
    """Clean up directory"""
    global createdFiles
    global outputFiles
    for eachFile in createdFiles:
        if eachFile not in outputFiles:
            Popen(['rm','-rf',eachFile])
            if args.verbose:
                print "Removed file: %s" % eachFile

def verifyCodedVerbatimsTab(sheets):
    """Check that a 'Coded Verbatims' sheet exists else prompt user to select from sheets found"""
    defaultSheetName = 'Coded Verbatims'
    if defaultSheetName not in sheets:
        print defaultSheetName+" sheet not found in workbook. Is one of these it?:\n"
        for sheet in sheets:
            print str(sheets.index(sheet)) - sheet
            vbf = raw_input('Enter number or "n" for no: ')
            if vbf not in range(0,len(sheets)):
                exit("Aborted.\n")
            else:
                global verbatimsKey
                verbatimsKey = sheets[int(vbf)]
    # Verbatims file found set it 
    else:
        global verbatimsKey
        verbatimsKey = sheets[sheets.index(defaultSheetName)]
    if args.verbose:
       print "Using %s as key file..." % verbatimsKey


def getCodes(workbook,sheet):
    """Go through a sheet and set up the codes question"""
    wb = xlrd.open_workbook(workbook)
    ws = wb.sheet_by_name(sheet)
    numRows = ws.nrows - 1
    numCols = ws.ncols - 1
    currRow = -1
    qInfo = [] # Array of dictionaries
    subnet = re.compile('.*SUBNET.*')
    net = re.compile('.*NET.*')
    parent = 0
    subparent = 0

    while currRow < numRows:
        currRow += 1
        row = ws.row(currRow)
        currCol = 0 # ignore first column since it's just types

        while currCol < numCols:
            currCol += 1
            # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank
            cell_type = ws.cell_type(currRow, currCol)
            cell_value = ws.cell_value(currRow, currCol)
            cell_value = safeString(cell_value)

            # Question Label from spreadsheet
            if currRow == 0 and cell_type != 0:
                qlabel = cell_value
                qInfo.append({'questionLabel': qlabel})

            # Question Text from spreadsheet
            elif currRow == 1 and currCol != 0 and cell_type != 0:
                qtext = safeString(cell_value)
                qInfo.append({'questionText':qtext})

            # All the other stuff
            else:
                if cell_value != 0:
                    # Last column should have row text
                    if currCol == numCols:
                        cell_value = safeString(cell_value)
                        if not ws.cell_type(currRow, currCol - 1) in [1,2]:
                            pass
                        else:
                            thisEl = {'label': int(ws.cell_value(currRow, currCol - 1)), 'text': safeString(cell_value), 'parent': parent, 'subparent': subparent}
                            # Nets
                            if re.match(net, cell_value) and not (re.match(subnet, cell_value)):
                                thisEl['type'] = 'net'
                                parent = thisEl['label']
                                subparent = 0
                            # Subnets
                            elif (re.match(subnet, cell_value)):
                                thisEl['type'] = 'subnet'
                                subparent = thisEl['label']
                            # Rows
                            else:
                                thisEl['type'] = 'row'
                            qInfo.append(thisEl)
    return qInfo
    
def vfunc(outputFile):
    """Create the virtual functions"""
    with open(outputFile,'a') as of:
        of.write("<exec when='virtualInit'>\ncodedOE = File('coded.txt', 'uuid')\n\ndef popVirt(vl, d, head):\n  for eachRow in vl.rows:\n    eachRow.val = d[head+eachRow.label]\n\ndef popCoded(codes):\n  for eachRow in codes.rows:\n    if eachRow.val:\n      data.attr('r' + eachRow.val).val = 1\n</exec>\n\n")
    


def makeCodesVirtual(info):
    """Take array of information, parse it, and create the question"""
    questionLabel = ""
    questionText = ""
    groups = []
    rows = []

    for eachD in info:
        if 'questionLabel' in eachD.keys():
            questionLabel = eachD['questionLabel']
        elif 'questionText' in eachD.keys():
            questionText = eachD['questionText']
        else:
            if eachD['type'] in ['net','subnet']:
                thisNet = '<group label="g'+str(eachD['label'])+'">'+eachD['text']+'</group>'
                groups.append(thisNet)
            elif eachD['type'] == 'row':
                thisRow = '<row label="r'+str(eachD['label'])+'" groups="g'+str(eachD['parent'])
                if eachD['subparent'] != 0 and eachD['parent'] == info[info.index(eachD) - 1]['parent']:
                    thisRow += ',g'+str(eachD['subparent'])
                thisRow += '">'+eachD['text']+'</row>'
                rows.append(thisRow)
    theQuestion = '<text label="'+questionLabel+'_codes" title="'+questionText+'">\n<virtual>\nOEData = codedOE.get(uuid)\n\nif OEData:\n  popVirt('+questionLabel+'_codes,OEData,"oe'+questionLabel.lower()+'")\n\n</virtual>\n'
    for eachG in groups:
        theQuestion += eachG +"\n"
    for eachRow in rows:
        theQuestion += eachRow + "\n"
    theQuestion += "</text>\n"
    return theQuestion
                    
                    
def arrayOptions(arr):
    """Prompt from array"""
    for i in range(0,len(arr)):
        print "%d - %s" % (i, arr[i])
    x = raw_input('Enter number or press enter: ')
    if int(x) in range(0,len(arr)):
        return x
    else:
        exit("Aborted.")
            
            
        
def tabDelimitedTemp(workbook,worksheet):
    """Create a temporary tab delimited file from the key file - this will have uuid and codes & later be gcated to create the final file"""
    wb = xlrd.open_workbook(workbook)
    ws = wb.sheet_by_name(worksheet)
    numRows = ws.nrows - 1
    numCols = ws.ncols - 1
    currRow = -1
    headerIndex = 0
    defaultQID = 'question id'
    defaultQCol = 0
    defaultUUIDCol = 1
    defaultVerbatimsCol = 2
    global verbatimsKey
    headers = []
    info = []
    while currRow < numRows:
        currRow += 1
#        row = worksheet.row(currRow)
        currCol = -1
        currQID = ""
        # Get headers
        if currRow == 0:
            while currCol < numCols:
                currCol +=1
                cell_value = ws.cell_value(currRow, currCol)
                headers.append(decoded(cell_value))
                headers = map(lambda x:x.lower(), headers)
            if defaultQID not in headers:
                print '%s not found in key file %s!\n\nI found these headers. Is it one of these?' % (defaultQID, verbatimsKey)
                defaultQCol = arrayOptions(headers)
            if 'uuid' not in headers:
                print 'uuid not found in key file %s!\n\nI found these headers. Is it one of these?' % verbatimsKey
                defaultUUIDCol = arrayOptions(headers)
            else:
                defaultUUIDCol = headers.index('uuid')
            if 'verbatim' not in headers:
                print 'verbatim not found in key file %s!\n\nI found these headers. Is it one of these?' % verbatimsKey
                defaultVerbatimsCol = arrayOptions(headers)
            else:
                defaultVerbatimsCol = headers.index('verbatim')
                
                
        else:
            thisD = {}
            colI = 1
            while currCol < numCols:
                currCol += 1
                # Cell Types: 0=Empty, 1=Text, 2=Number, 3=Date, 4=Boolean, 5=Error, 6=Blank
                cell_type = ws.cell_type(currRow, currCol)
                cell_value = ws.cell_value(currRow, currCol)
                # Check question col
                if currCol == defaultQCol and cell_value != currQID:
                    currQID = cell_value
                    info.append({currQID: []})
                else:
                    
                    if currCol == defaultUUIDCol:
                        thisD['uuid'] = cell_value
                    elif currCol == defaultVerbatimsCol:
                        thisD['verbatims'] = cell_value
                    else:
                        if cell_type not in [0,6]:
                            thisD['code'+str(colI)] = str(int(cell_value))
                            colI += 1
            info[-1][currQID].append(thisD)
    tempFileFromArray(info)
                    

    
def tempFileFromArray(arrayOfInfo):
    """Create temp file - array in format [{key: array[{key:value,key:value}]}, {key: array[{key:value,key:value}]}]"""
    global createdFiles
    headers = ['qlabel','uuid','codes']
    for item in arrayOfInfo:
        # item key is file
        filename = 'temp-'+str(item.keys()[0])+'.txt'
        if filename not in createdFiles:
            createdFiles.append(filename)
            with open(filename,'w') as f:
                for eachHeader in headers:
                    if headers.index(eachHeader) < len(headers) - 1:
                        f.write(eachHeader+"\t")
                    else:
                        f.write(eachHeader+"\n")
        else:
            # item value is array of headers and corresponding values
            values = item.values()[0][0].values()
            with open(filename,'a') as f:
                f.write(decoded(values['qlabel'])
                f.write(decoded(values['uuid']))
                for eachVal in values:
                    if not ( values['qlabel'] or values['uuid'] ):
                        if values.index(eachVal) < len(values) - 1:
                            try:
                                f.write(decoded(eachVal)+"\t")
                            except:
                                f.write("\t")
                        else:
                            f.write(eachVal+"\n")



def runAll(spreadsheet,out):
    """Do everything"""
    global verbatimsKey
    if not (args.f):
        exit("Specify file name with oe codes. oe_code.py -h for help.")
    checkExistance(spreadsheet)
    sheets = getWorksheets(spreadsheet)
    verifyCodedVerbatimsTab(sheets)

    with open(out,'w') as of:
        for eachSheet in sheets:
            if eachSheet != verbatimsKey:
                codes = getCodes(spreadsheet, eachSheet)
                toWrite = makeCodesVirtual(codes)
                
                #toWrite = makeCodesVirtual(codes).encode('utf-8')
                of.write(toWrite)
    vfunc(out)
    tabDelimitedTemp(args.f, verbatimsKey)

out = 'output.xml'
runAll(args.f, out)
