#!/usr/bin/perl
# Author: Victor Hernandez (vhernandez@decipherinc.com)
# Date: June 26, 2014
# Description: This file is going to do MAGIC and make the turf EVERYTHING for you :)

use lib "/home/vhernandez/perl5/lib/perl5";
use lib "/home/vhernandez/perl5";
use lib '--install_base "/home/vhernandez/perl5"';
use lib "/home/vhernandez/perl5";
exec(eval `perl -I ~/perl5/lib/perl5 -Mlocal::lib`);
use strict;
use Getopt::Long();
use autodie; #Sad Face
use Spreadsheet::WriteExcel; #Excel for TURF Simulator
use Excel::Writer::XLSX;
use Scalar::Util qw(looks_like_number); # This is used to verify numeric input for some options
#sub routine for printing usage options
sub usage {
    my $message = $_[0];
    if (defined $message && length $message) {
      $message .= "\n"
	  unless $message =~ /\n$/;
    }

    my $command = $0;
    $command =~ s#^.*/##;

   print STDERR (
      $message,
      "usage: $command -q questionLabel [-h] [-s] [-v] [-oQ] [-oD] [-rc #ofRowCombinations (default is 5)] [-c #numberOfCombination attempts (default is 100)] [-fQ turfQuestionRows.txt] [-fD turf-data.txt] [-fX spreadSheetFile.xls]\n" .
      "       ...\n" .
      "       ...\n" .
      "       ...\n"
       );

   die("\n")
}

# Option Variables
my $help;
my $questionLabel;
my $verbose;
my $questionFile;
my $dataFile;
my $rowCombinations;
my $questionFileName;
my $dataFileName;
my $xlName;
my $combinations;
my $skipSimulator;
#Handle options
Getopt::Long::GetOptions('h' => \$help, 'help' => \$help, 'v' => \$verbose, 'verbose' => \$verbose, 'q=s' => \$questionLabel, 'rc=i' => \$rowCombinations, 'c=i' => \$combinations, 'oQ' => \$questionFile, 'oD' => \$dataFile, 's' => \$skipSimulator, 'fQ=s' => \$questionFileName, 'fD=s' => \$dataFileName, 'fX=s' => \$xlName);

#Set Defaults if needed
if ($help){
    usage();
}

if (defined $rowCombinations){
    if (looks_like_number($rowCombinations)){
	if ($verbose){
	    print "Row Combinations to be generated: $rowCombinations\n";
	}
    }
    else{
	die("The -rc option takes a numeric value!\n");
    }
}
else{
    $rowCombinations = 5;
}

if (!defined $questionFile){
    $questionFile = "turfquestions.txt";
}

# handle a specified/unspecified number of combinations for the script
if (defined $combinations){
    if (looks_like_number($combinations)){
	if ($verbose){
	    print "Combinations to be generated: $combinations\n";
	}
    }
    else{
	die("The -c option takes a numeric value!\n");
    }
}
else{
    $combinations = 100;
}


usage("Please specify the TURF question label with the -q option")
    unless defined($questionLabel);

# Start doing work 
if ($verbose){
    print "Thank you for using this AWESOME TURFulator!\n";
}

# Backup the survey file
print "Creating a backup of your current survey @ survey.turfulate.backup....";
system('cp survey.xml survey.turfulate.backup');

# Open the survey file and look for the question label provided
if ($verbose){
    print "Opening survey.xml.\n";
}


# Let's take a look at the survey file
my $file = "survey.xml"; 
open(XML, $file);
my @xml = <XML>;
close(XML);
my $offset = 0;

if ($verbose){
    print "Searching for question with label $questionLabel...\n";
}

my $rowCount;
my @rowArray = @xml;

# Count the number of rows in the turf question
while (my $line = shift @rowArray){
# get rid of the questions we dont care about at the beginning of the array
    shift @rowArray if $line =~ m/label="\Q$questionLabel"/;
# find the TURF question and count the number of rows
    if ($line =~ m/label="\Q$questionLabel"/){
	if ($verbose){
	    print "Determining question rows...\n";
	}
	while ($line = shift @rowArray){
	    if ($line =~ m/<row/){
		$rowCount++;
	    }

	    last if $line =~ m/<suspend.*/;
	}
    }
}

# Let's make sure a turf questions hasn't already been added....
foreach my $line (@xml){
    my $tquestion = $questionLabel."_CB";
    if ($line =~ m/label="\Q$tquestion"/){
	print "TURF Question has already been added...\n";
    }
}

#Create the new question for turf-analyze
foreach my $line (@xml){
    if ($line =~ m/label="\Q$questionLabel"/){
	if ($verbose){
	    print "Question Found!\n";
	}
	my @array = @xml;        # Copy
	while (my $token = shift @array) {
	    shift @array if $token !~ m/<suspend\/>/; # consumes the next element
	    $offset++;
	    if ($token =~ m/<suspend\/>/){
		$offset++;
		if ($verbose){
		    print "Generating New Question ".$questionLabel."_CB\n";
		    print "My offset: $offset\n";
		}
		last;   
		splice @xml, $offset, 0,  "<checkbox label=\"".$questionLabel."_CB\" atleast=\"1\" title=\"Checkbox $questionLabel\" onLoad=\"copy('$questionLabel', rows=True)\" virtual=\"setIfEqual($questionLabel ,0)\" where=\"none,notdp\"/>\n\n";
	    }
	}
    }
    $offset++;
}

# Actually Transfer Questions to file
$file = "newsurvey.xml";
open (OUT, ">", $file);
foreach my $line (@xml){
    print OUT $line;
}
if ($verbose){
    print "Question successfully added.\n";
}


# Run the turf script to generate rows
my @combos;
my $i = 0;
while ($rowCombinations > 0){
    $combos[$i] = ($i + 1)." ";
    $i++;
    $rowCombinations--;
}

open(my $TQ, ">", $questionFile);

# run turf-analyze
my $ta = "python /home/jaminb/v2/temp/briang/python/combo.py";
if ($verbose){
    print "Executing turf-analyze...\n";
    print "for x in ", @combos, "; do combo.py . ", $questionLabel."_CB all ", $combinations, " \$x | combo.py; done > ", $questionFile."\n";
}

system("for x in @combos; do turf-analyze . ".$questionLabel."_CB all $combinations \$x | $ta; done > $questionFile");

close ($TQ);

if ($verbose){
    print "\n\nTURF rows successfully generated.\n";
}

# Now let's create an ORDERED row turf question
open(QFile, "<", $questionFile); # Open our new question file for reading;
print "Processing file\n";
my @fileArray = <QFile>; #Copy file contents to array
close(QFile);
shift @fileArray;
 #Individual Rows on TURF question that will be used to created the ordered version
my @singleRows;
$i = 0; #Reset my dummy variable

foreach my $line(@fileArray){
    if ($line =~ m/<!--.*-->/){
	last;
    }
    push(@singleRows,$line);
}

# Create the ordered version
my @orderedRows; 
my $numRows = scalar(@singleRows);
print "NUM ROWS: $numRows\n\n";

while ($i <= $numRows){
    foreach my $row (@singleRows){
	if ($row =~ m/.*\Qlabel='$i'/){
	    $orderedRows[$i] = $row;	    
	}
    }
    if ($i == $numRows){
	$orderedRows[$i] = $fileArray[$i-1];
    }
    $i++;
}

if ($verbose){
    print "Ordered rows successfully generated...\n";
}
my $numberOfRows = scalar(@orderedRows); #This is used to determine the number of rows for steps in the simulator

unshift(@orderedRows,"<checkbox label=\"$questionLabel"."_ORDERED_TURF\" atleast=\"1\" title=\"Turf Combination of 1 Item - $questionLabel\" virtual=\"combo($questionLabel, 0)\">"."\n");
push(@orderedRows,"</checkbox>\n\n");

foreach my $line (@xml){
    print OUT $line;
}

#Now we create FULL questions out of the rows created
$file = "turfquestions.txt";
open(XML, $file);
@xml = <XML>;
close(XML);
$offset = 0; #Reset my counter 
my $tqText;

# Go through the questions file
foreach my $line (@xml){
    # If line is a descriptor replace with actual question tag
    if ($line =~ m/<!-- combo count:/){
	# This is the combo count number 
	$line =~ tr/0-9//cd;
	if ($verbose){
	    print "Adding Question for $line Combinations...\n";
	}
#Let's make it be grammatically correct

	if ($line =~ m/1/){
	    $tqText = "<checkbox label=$questionLabel"."_".$line."_TURF\" atleast=\"1\" title=\"Turf Combination of $line Item - $questionLabel\" virtual=\"combo($questionLabel ,0)\">"."\n";
	}
	else {
	       $tqText = "</checkbox>\n\n<checkbox label=\"$questionLabel"."_$line"."_TURF\" atleast=\"1\" title=\"Turf Combination of $line Items - $questionLabel\" virtual=\"combo($questionLabel ,0)\">\n";
	}
	$line =~ s/.*/$tqText/;
	$xml[$offset] = $line;

    }
    $offset++;
}

#close the question
$xml[scalar(@xml)+1] = "</checkbox>\n";

unshift(@xml,@orderedRows);

open(my $myTest, ">", "turfuquestion.txt");


#Now we add the new generated questions to the survey file

if ($verbose){
    print "Adding Turf Questions.....\n\n";
}

#add the questions into the file 
$file = "survey.xml"; 
open(XML, $file);
my @survey = <XML>;
close(XML);
$offset=0;

my $tQuestion = $questionLabel."_CB";

#Find the initial question & add TURF QUESTIONS
foreach my $line (@survey){
    if ($line =~ m/\Q$tQuestion/){
	#Add the TURF Questions
	splice @survey,$offset+1,0,@xml;
	last;
    }
    $offset++;
}

#Print the final product for the survey :)
print $myTest @survey;


if (! $skipSimulator){
#Generate Data to be used later in excel spreadsheet
system("generate . qualified tab uuid list \$(seq 1 18 \| sed 's/^/".$questionLabel."r/' \| tr '\n' ' ') > turf-data.txt");
my $dataFile = "test.txt";
$offset = 0; #reset offset

# Create a new workbook called simple.xls and add a worksheet.
my $workbook  = Excel::Writer::XLSX->new('simulator.xlsx');
my $simulator = $workbook->add_worksheet("Sim");
my $data = $workbook->add_worksheet("Data");
my $Config = $workbook->add_worksheet("Config");

#Set up the data sheet from the data file
$data->write(0,0,"uuid");
$data->write(0,1,"list");
open(TDATA,"<",$dataFile);
my %possibleLists; #store different list values
my @unique; #store the unique values
my @letters = ("C".."Z"); #Used to set up data file
my @allLetters = ("A".."Z");            # Store all letters - used for spreadsheet col labels
my $dataEnabled;
my $temp;                               # temp variable used as "scratch"
my $dataVal;
my $formulaString;
my $pos;                                # Position - used for writing formulas
my $ind1 = 0;
my $firstSimulatorCol;                  # This will store the column label of the first "simulator" data portion -- NOT USED ATM since it's not being set up to support overall TURFs yet
my $lastSimulatorCol;                   # Similar to the above but the last
my $percentageColumn;                   # used to identify the column where percentage formulas should be written
my $percentageColLabel;                 # used to identify the label for the percentage column (AA, AZ, BA, whatever it is.)

#Set up the data & config sheet
#read data file line by line
foreach my $line (<TDATA>){
    # This array has the data split by columns. The scalar value can be used to determine the number of columns
    my @lineInfo = split("\t",$line);     
    $percentageColumn = scalar(@lineInfo) * 2 - 1;

    # This offset 0 block sets up the labeling in the spreadsheet
    if ($offset == 0){
	for (my $i = 1 ; $i < scalar(@lineInfo)-1; $i++){
	    # Write the question labels
	    $data->write(0, $i+1, $lineInfo[$i]);
	    # Write the enabled value which will be controled by the checkboxes
	    $data->write(1, $i+1, "FALSE");
	    # Repeat Question labels for "simulator data" section
	    $data->write(0, (scalar(@lineInfo)-1) + ($i+1), $lineInfo[$i]);
	}

	$line = <TDATA>; #skip the headers
    }

    # Now write the actual data
    else{
	# Write the UUIDs
	$data->write($offset+1,0, $lineInfo[0]);
	# Write the list values 
	$data->write($offset+1,1,$lineInfo[scalar(@lineInfo)-1]);
	
#####     NOTE :  WILL WANT TO ALLOW FOR SECOND TIER SEGMENTING FOR OVERALL TURFS ####


	# Get unique list values used in config file
	if (! $possibleLists{$lineInfo[scalar(@lineInfo)-1]}){
	    push(@unique,$lineInfo[scalar(@lineInfo)-1]);
	    $possibleLists{$lineInfo[scalar(@lineInfo)-1]} = 1;
	}

	# Write the actual data from the generated file and the formualas used for the simulator
	for (my $i = 1 ; $i < scalar(@lineInfo)-1; $i++){
	    # Write the raw data
	    $data->write($offset+1, $i+1, $lineInfo[$i]);

	    #Data, if enabled set value	 
	    $dataEnabled = $letters[$i-1]."\$2"; # Value True or False? C$2, D$2, etc
	    $temp = $i + 2; 
	    $dataVal = $letters[$i].$temp; #C3,D3,etc.
	    $pos = $allLetters[scalar(@lineInfo)+$i].$temp;

	    # Handle iterator being grater then 26 (ie letter Z in spreadsheet) 
	    if ( ( scalar(@lineInfo)+$i ) > scalar(@allLetters) ){
		$temp = scalar(@allLetters) - scalar(@lineInfo) + $i;
		$percentageColLabel = $allLetters[0].$allLetters[$percentageColumn-26]; # This is used for the percentage column, it represents the first row of data in the percentage calculation
		$pos = $allLetters[0].$allLetters[$temp-24].$temp;		
	    }
	    my $FORMULA = "IF(AND(".$dataEnabled.",COUNTIF(Config!\$A:\$A,".$dataVal.")),1,0)";
	    $data->write_formula($offset+1, ( scalar(@lineInfo) + $i ),$FORMULA);
	# This only supports list splits --
	    my $tempOffset = $offset + 2;
	    $firstSimulatorCol = $allLetters[1 + scalar(@lineInfo)].$tempOffset;
	    
	    my $percentageFormula = "IF(COUNTIF(Config!\$D:\$D,\$B".$tempOffset."),IF(SUM(".$firstSimulatorCol.":".$percentageColLabel.$tempOffset."),1,0),\"\")";
#	    print $percentageFormula."\n";
	    $data->write_formula($offset+1, $percentageColumn, $percentageFormula);
	}


    }
    $offset++;

}

# Write the percentage column 	
# The percentage formula is based on the number of data rows which is stored as the offset + 1
$offset++; 

#Start and end of percentage formula cells column
my $percentageEnd = $percentageColLabel.$offset;
my $percentageStart = $percentageColLabel."3";
my $format = $workbook->add_format();
$format->set_num_format('0.0%');

#Set up the formula for the percentage calculation
my $percentageCalculationFormula = "IF(COUNT(".$percentageStart.":".$percentageEnd.")>0,AVERAGE(".$percentageStart.":".$percentageEnd."),0)";

# Can uncomment the following to debug formula 
# print "MY FORMULA: $percentageCalculationFormula\n";

$data->write_formula(1, $percentageColumn, $percentageCalculationFormula, $format);


#Enabled Values
$Config->write(0,0,"Enabled Values");
$Config->write(1,0,5);

#Enabled
$Config->write(0,2,"Enabled");

#Segments
$offset = 0;
$Config->write(0,1,"Potential \"Segment\" Values");
foreach my $value (@unique){
    $Config->write($offset+1,1,$value );
    $Config->write($offset+1,2,"FALSE");
    $offset++;
}


#Enabled Values
$Config->write(0,3,"Enabled Segment Values");
$offset = 1;

#Write Config Set up on Config sheet (ie, if enabled, enabled seg value is activated)
while ($offset <= scalar(@unique)){
    my $num = $offset + 1;
    my $seg = "D".$num;
    my $segVal = "C".$num;
    $Config->write($seg,"=IF(".$segVal."=TRUE,1,\"\")");
    $offset++;
}

# now we close the workbook so we can use it for the next part 
$workbook->close();



# Now for the actual simulator :/ here the fun begins :(

# First let's make a directory so we can play with the xlsx file
#system("mkdir tempXLSX");
#system("cp simulator.xlsx tempXLSX"); # copy the simulator xlsx file into the directory
#system("unzip tempXLSX/simulator.xlsx -d tempXLSX"); #unzip the file so I can play with the macros :/
#system("rm tempXLSX/simulator.xlsx"); # this is to prevent errors when zipping files back up
#system("mkdir tempXLSX/xl/drawings"); # This will hold the file with the macros
#system("touch tempXLSX/xl/drawings/vmlDrawing1.vml");
#
#open(DRAWING, ">", "tempXLSX/xl/drawings/vmlDrawing1.vml");
#print DRAWING "<xml xmlns:v=\"urn:schemas-microsoft-com:vml\"\n xmlns:o=\"urn:schemas-microsoft-com:office:office\"\n xmlns:x=\"urn:schemas-microsoft-com:office:excel\">\n <o:shapelayout v:ext=\"edit\">\n  <o:idmap v:ext=\"edit\" data=\"1\"/>\n </o:shapelayout><v:shapetype id=\"_x0000_t201\" coordsize=\"21600,21600\" o:spt=\"201\"\n  path=\"m,l,21600r21600,l21600,xe\">\n  <v:stroke joinstyle=\"miter\"/>\n  <v:path shadowok=\"f\" o:extrusionok=\"f\" strokeok=\"f\" fillok=\"f\" o:connecttype=\"rect\"/>\n  <o:lock v:ext=\"edit\" shapetype=\"t\"/>\n </v:shapetype><v:shape id=\"_x0000_s1025\" type=\"#_x0000_t201\" style='position:absolute;\n    margin-left:39pt;margin-top:8.25pt;width:61.5pt;height:16.5pt;z-index:1;\n  mso-wrap-style:tight' filled=\"f\" fillcolor=\"window [65]\" stroked=\"f\"\n  strokecolor=\"windowText [64]\" o:insetmode=\"auto\">\n  <v:path shadowok=\"t\" strokeok=\"t\" fillok=\"t\"/>\n  <o:lock v:ext=\"edit\" rotation=\"t\"/>\n  <v:textbox style='mso-direction-alt:auto' o:singleclick=\"f\">\n   <div style='text-align:left'><font face=\"Tahoma\" size=\"160\" color=\"auto\">Check\n   Box 1</font></div>\n  </v:textbox>\n  <x:ClientData ObjectType=\"Checkbox\">\n   <x:SizeWithCells/>\n   <x:Anchor>\n    0, 52, 0, 11, 2, 6, 1, 13</x:Anchor>\n   <x:AutoFill>False</x:AutoFill>\n   <x:AutoLine>False</x:AutoLine>\n   <x:TextVAlign>Center</x:TextVAlign>\n   <x:Checked>1</x:Checked>\n   <x:NoThreeD/>\n  </x:ClientData>\n </v:shape></xml>";
#
#system(`zip -r tempXLSX/simulator.xlsx tempXLSX/*`);
#system(`mv tempXLSX/simulator.xlsx .`);
#system(`rm -rf tempXLSX`);
}
