== Timings for 9000 records, in micro-seconds per record == 
Q9f_COMBO_6       286   22.26
Q9f_COMBO_5       238   18.52
Q9f_COMBO_4       191   14.85
Q9f_COMBO_3       143   11.14
vos                90    7.04
vbrowser           66    5.14
setup              65    5.06
Q9f_ORDERED_TURF       21    1.69
Q9f_COMBO_2        19    1.54
write              17    1.35
Q9f_CB             17    1.33
vlist              15    1.21
vmobiledevice       11    0.88
vmobileos           9    0.75
voqtable1           8    0.64
vqtable1            6    0.50
vqtable2            6    0.50
voqtable2           4    0.37
TOTAL            1288 (776.33 records per second)
