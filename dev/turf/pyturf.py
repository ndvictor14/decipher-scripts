#!/usr/bin/python

import xlsxwriter
import os 
import subprocess # used for running command line among other things
import re
import sys
import operator
from collections import defaultdict # Used to create dictionary of lists (mainly for config information)
from optparse import OptionParser
from string import ascii_uppercase
from os.path import join

##############################################################
# Written by Victor Hernandez (vhernandez@decipherinc.com)   #
##############################################################



# The purpose of this script is to repleace turfulate.pl so that it is more readable and maintable by Decipher employees (since they don't know PERL :P

# This is used for syntax wrapping - single quoting vs double quoting
def smart_wrap(x):
    # If argument has single quotes around it, wrap it in double quotes
    if re.match(r'.*\'.*',x):
        return ('"%s"' % x)
    else:
        return ("\'%s\'" % x)

# This is used for spreadsheet writing locations
def spreadCol(num):
    alphabet = ascii_uppercase
    if num < 26:
        return alphabet[num]
    else:
        return str(alphabet[num/26-1])+str(alphabet[num%26])

# Used to get file line count
def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1


def cleanArray(arr):
    clean = []
    for item in arr:
        clean.append(''.join(item.split()))
    return clean

keyIndex = {} # Keep track of indexes of keys
# This reorders the data file - speeds up data writing to spreadsheets
def reorder(thisFile):
    dataCounter = 1 # Awk command see $0 as the entire row 
    questionCols = []
    mySegs = segNames


    with open(thisFile) as messy:
        line = next(messy)
        cols = line.split('\t')
        cols = cleanArray(cols)

        for eachCol in cols:
            # If the column isn't the key column , keep track of it's index in the array
            if eachCol not in mySegs and eachCol not in ['uuid']:
                dataCounter += 1
                questionCols.append(dataCounter)
            # If the column IS the key index keep track of it's index in "keyIndex"
            elif eachCol in mySegs:
                keyIndex[eachCol] = dataCounter
                dataCounter+=1

        # Set up the command based on the information obtained
        command=' '
        for colIndex in questionCols:
            command = command+"\"\\t\"$"+str(colIndex)+" "
    
    awkKI = ""
    for keyName,index in keyIndex.iteritems():
        awkKI = awkKI + "\"\\t\"$"+str(index+1)

    reorderedFile = options.path+"reordered.txt"    
    awkCommand = "awk '{print $1"+awkKI+command+"}' "+thisFile+" > "+reorderedFile
    if options.verbose:
        print "Running: "+awkCommand
        print "Replacing turf-data.txt with reordered file...."
    subprocess.call(awkCommand,shell=True)

    subprocess.call('mv '+reorderedFile+options.path+thisFile,shell=True)



parser = OptionParser()
parser.add_option("-p", "--path", dest="path", default=".",
                      help="Survey Path - defaults to current directory")
parser.add_option("-q", "--question label", dest="question",
                      help="Label of question to run script on")
parser.add_option("-v", "--verbose", dest="verbose", action="store_true",
                      help="Verbose output")
parser.add_option("-e", "--python expression for generate", dest="expression",default="qualified",
                      help="Python expression used in generate - for example if you have multiple lists and only want the turf data for one do -e 'list==\"3\"' (not the\"\" around the entire argument!")
parser.add_option("-s", "--segment", dest="segs",default="list",
                      help="Variable used for segments - defaults to list")


(options, args) = parser.parse_args()

if "/" not in options.path[-1]:
    options.path = options.path + "/"
segNames = options.segs.split(',')

# COMMENT OUT FOR SIMULATOR DEBUGGING
# Make sure a question label was passed
if not options.question:
    exit("You must specify a question label to TURF-ulate....\npyturf -h for more info. \n Good-Bye!\n")



#REGEX to find both single quoted or double quoted question lables (cuz programmers are inconsistent)
labelDouble = re.compile('.*label="'+options.question+'".*')
labelSingle = re.compile(".*label='"+options.question+"'.*")


#REGEX to find rows
rows = re.compile('.*<row .*</row>')

#REGEX to determine when end of question has been reached
suspendEnd = re.compile(".*<suspend/>.*")


#REGEX to ensure TURF question has already been added 
TQ = options.question+"_CB"
TQ.rstrip(' ')
TurfQ = re.compile('.*%s.*'%TQ)

# temp variables for temporary tasks
temp = False
index = 0
counter = 0

turfRows = 0 #Counts the numbers of rows in the TURF question
virtual = re.compile('.*virtual=\"combo.*|.*virtual=\'combo.*')
existingVirtuals = []
print "Thank you for using this awesome TURFulator :)"

if options.verbose:
    print "Searching for question " + options.question + "..."


# Open the survey xml read line by line
surveyFile = options.path + "survey.xml"
with open(surveyFile) as survey:
    for line in survey:
        counter += 1

        # Look for the specified question label
        if re.match(labelSingle,line) or re.match(labelDouble,line):
            print line
            counter += 1
            if options.verbose:
                print "Question found on line " + str(counter)
                print "Determining question rows....\n"

            # Determine the number of rows to be used in the turf-analyze script
            while not re.match(suspendEnd,line):
                line = next(survey)
                counter += 1
                while re.match(rows,line):
                    turfRows += 1
                    counter += 1
                    line = next(survey)
                index = counter


        # If TURF question already created go to the next step
        if re.match(TurfQ,line):
            temp = True
            print "TURF QUESTION ALREADY CREATED! RUNNING ANALYSIS....\n"
        if re.match(virtual,line):
            existingVirtuals.append(line)

if len(existingVirtuals) > 0:
    print "TURF QUESTIONS HAVE ALREADY BEEN CREATED!!\n\nIf you need to re run the script for this question please remove it: \n\n"
    for v in existingVirtuals:
        print "\t"+v
    print "Skipping to data generation..."


# If the turf questions wasn't already added, add it into the survey so that the scripts can run
if not temp:
    newQ = "<checkbox label=\""+TQ+"\" atleast=\"1\" title=\"Checkbox "+options.question+"\" onLoad=\"copy('"+options.question+"', rows=True)\" virtual=\"setIfEqual("+options.question+",0)\" where=\"none,notdp\"/>\n\n";
    survey = open(surveyFile,"r")
    content = survey.readlines()
    survey.close()
    content.insert(index,newQ)
    survey = open(surveyFile,"w")
    content = "".join(content)
    survey.write(content)
    survey.close()


# Run the scripts and get the stuff we need 
if len(existingVirtuals) == 0:
    tqFile = options.path+"turfquestions.txt"
    command = "for x in 1 2 3 4 5; do turf-analyze "+surveyFile+" "+TQ+" all 100 $x | /home/jaminb/v2/temp/victor/scripts/./combo.py; done > "+tqFile
    if options.verbose:
        print "Running: " + command
    
    subprocess.call(command,shell=True)
    print command
    # These are used to create the ordered turf question
    
    combo2 = re.compile('<!--.*combo.*2.*-->')
    turfQuestionRows = {}
    
    counter = 0
    
    # At this point we have the combo rows in "turfquestions.txt"
    with open(tqFile) as newQs:
        next(newQs) # Skip header
        for line in newQs:
            if re.match(combo2,line):
                break            
            turfQuestionRows[counter] = line
            counter += 1
           
    # turfQuestionRows now has combo 1 rows
    
    if options.verbose:
        print "Creating ordered Rows"
    
    ordered = {}
    finalOrderedRows = []
    
    # Store the last row so that it can be removed from the array and sorted by number in ascending order
    noneRow = turfQuestionRows[counter-1]
    del turfQuestionRows[counter-1]
    
    # Strip the rows so that they can be ordered by labels
    for rowIndex,row in turfQuestionRows.iteritems():
        temp = re.sub(r'.*label=\'','',row)
        temp = re.sub(r'\'.*','',temp)
        ordered[rowIndex] = int(temp.rstrip('\n'))
    
    # sort the dictionary containing index from original rows
    ordered = sorted(ordered.items(), key=operator.itemgetter(1))
    
    # Match the keys to th row text and append them to the ordered array 
    for eachKey in dict(ordered).values():
        for eachRowIndex,eachRowText in turfQuestionRows.iteritems():
            if eachKey == eachRowIndex:
                finalOrderedRows.append(eachRowText)
    
    # Prepend header for replacement
    finalOrderedRows.insert(0,"<!-- combo ordered -- >\n")
    
    # Add the none row previously removed
    finalOrderedRows.append(noneRow)
    
    # Add ordered rows to turfquestions.txt
    turfFile = open(tqFile,"r")
    content = turfFile.readlines()
    turfFile.close()
    counter = 0
    for rowText in finalOrderedRows:
        content.insert(counter, rowText)
        counter += 1
    turfFile = open(tqFile,"w")
    content = "".join(content)
    turfFile.write(content)
    turfFile.close()
    
    counter = 1
    
    combo = re.compile('<!--.*combo.*>')
    orderedRegex = re.compile('<!--.*ordered.*')
    
    # Update headers to be questions tags
    turfFile = open(tqFile,"r")
    content = turfFile.readlines()
    newContent = []
    turfFile.close()
    for line in content:
        if re.match(orderedRegex,line):
            if options.verbose:
                print "ORDERED MATCH FOUND\n\n"
                print "Creating ordered question...\n"
    
            line = orderedRegex.sub("<checkbox label=\""+options.question+"_ORDERED_TURF\" atleast=\"1\" title=\"Ordered TURF Rows\" virtual=\"combo("+options.question+",0)\">",line)
            counter+=1
                
        elif re.match(combo,line):
            line = combo.sub("</checkbox>\n\n<checkbox label=\""+options.question+"_COMBO_"+str(counter)+"\" atleast=\"1\" title=\"Combination of "+str(counter)+"\"  virtual=\"combo("+options.question+",0)\">",line)
            counter+=1
        
        newContent.append(line)
    
    # Update results in the file
    turfFile = open(tqFile,"w")
    content = "".join(newContent)
    turfFile.write(content)
    turfFile.close()
            
    
    
    
    with open(tqFile,"a+") as questions:
        for line in questions:
            pass
        last = line
        if not re.match(r'.*</checkbox>.*',last):
            questions.write("</checkbox>\n") # Close the last TURF question
        
    
    
    # Add TURF Questions to survey
    turfFile = open(tqFile,"r")
    content = turfFile.readlines()
    turfFile.close()
    content = "".join(content)
    surveyFile = open(surveyFile,"r")
    surveyContent = surveyFile.readlines()
    surveyFile.close()
    print surveyContent[index+2]
    print surveyContent[index+1]
    print surveyContent[index]
    surveyContent.insert(index+2,content)
    surveyFile = open(surveyFile,"w")
    surveyContent = "".join(surveyContent)
    surveyFile.write(surveyContent)
    surveyFile.close()



# Generate turf data
dirtySegs = options.segs.split(',')
cleanSegs = " "
for seg in dirtySegs:
    cleanSegs = cleanSegs + " " +seg

# Make sure qualified is one of the expressions for the generate command! 
if "qualified" not in options.expression:
    options.expression = options.expression[:0] + "qualified and " + options.expression[0:]
tdFile = options.path+"turf-data.txt"
generateCommand = "generate -p "+options.path+" "+smart_wrap(options.expression)+" tab uuid "+cleanSegs+ " $(seq 1 "+str(turfRows)+" | sed 's/^/"+options.question+"r/' | tr '\\n' ' ') > "+tdFile
if options.verbose:
    print "Running: "+generateCommand
subprocess.call(generateCommand,shell=True)

# UNTIL HERE FOR SIMULATOR DEBUGGING

# Create the simulator
simFile = options.path+"simulator.xlsx"
simulator = xlsxwriter.Workbook(simFile)
SimulatorSheet = simulator.add_worksheet('Simulator')
DataSheet = simulator.add_worksheet('Data')
ConfigSheet = simulator.add_worksheet('Config')

curRow = 3
index = 0 # Used to find the index of the key
letters = ascii_uppercase # Used for formula writing

# Add the data to the Data Sheet

dataRows = file_len(tdFile)

# Re arrange file columns based on the index of the key
reorder(tdFile)
segs = len(options.segs.split(','))

segNames.insert(0,'uuid')
segDict = defaultdict(list)             # Keep a dictionary of the possible seg values to use in the config sheet
with open(tdFile) as data:
    header = next(data)
    headerCols = header.split('\t')
    headerCols = cleanArray(headerCols)
    # Based on the number of keys, we need to determine which headerCols are "static"
    for i in range(0,segs+1):
        DataSheet.write(letters[i]+"1",headerCols[i])

    exclusions = []
    for i in range(1,segs+1):
        exclusions.append(len(headerCols)+i)
    exclusions.append(len(headerCols))
    print exclusions
    # headerCols has the question labels so we add them to the first row of the data sheet
    for i in range(segs+1,len(headerCols)*2):
        # Ignore key headers
        if i not in exclusions:
            if (i > len(headerCols)+segs):
                print "RIGHT: "+str(i)+": "+headerCols[i%len(headerCols)]
                DataSheet.write(spreadCol(i-segs)+"1",headerCols[i%len(headerCols)])
            else:
                print "LEFT: "+str(i)+": "+headerCols[i%len(headerCols)]
                # Print the data cols and the checkbox trigger values
                DataSheet.write(spreadCol(i)+"1",headerCols[i%len(headerCols)])
                DataSheet.write(spreadCol(i)+"2",'FALSE')


    percentageCol = spreadCol(len(headerCols)*2-segs)
    percentageFormula ="=IF(COUNT("+percentageCol+"3:"+percentageCol+str(int(dataRows+1))+">0),AVERAGE("+percentageCol+"3:"+percentageCol+str(int(dataRows+1))+"),0)"

    DataSheet.write_formula(percentageCol+"2",percentageFormula)

        # Now we actually add the data from the file
    spreadRow = 3  # Starts at 3 to account for the stuff we already added (headers, T/F)
    for line in data:
        cols = line.split('\t')
        cols = cleanArray(cols)
        for colIndex in range(0,len(cols)):
            # Populate segDict
            if headerCols[colIndex] in keyIndex.keys(): 
                segDict[headerCols[colIndex]].append(cols[colIndex])
            DataSheet.write(spreadCol(colIndex)+str(spreadRow),cols[colIndex])

            # Write the calculation formulas 
            if (colIndex < (len(cols) - segs - 1) ):
                calculationFormula = "=IF(AND("+spreadCol(colIndex+segs+1)+"2,COUNTIF(Config!$A:$A,"+spreadCol(colIndex+segs+1)+str(curRow)+")),1,0)"
                DataSheet.write_formula(spreadCol(colIndex+len(cols)+1)+str(spreadRow),calculationFormula)
        rowPercentageFormula = "=IF(COUNTIF(Config!$D:$D,$B"+str(curRow)+"),IF(SUM("+spreadCol((len(cols)+1))+str(curRow)+":"+spreadCol(len(cols)*2-2)+str(curRow)+"),1,0),\"\")"
        DataSheet.write_formula( spreadCol( len(cols) * 2 - segs ) + str(spreadRow) ,rowPercentageFormula)
        curRow+=1     
        spreadRow += 1
    data.close()
# All the data is in the data sheet so now the easier part - the config! :D
ConfigSheet.write(spreadCol(0)+"1","Enabled Values")
ConfigSheet.write(spreadCol(0)+"2",5)

ConfigCol = 1


# Clean up segDict so that there aren't any duplicates and sort them
for eachKey,eachList in segDict.iteritems():
    segDict[eachKey] = sorted(list(set(eachList)))


# This is only done to speed up the process of implementing potential "multiple" segs (thankfully everytime Mark asks he cancels the request :P )
for i in range(1,segs+1):
    ConfigSheet.write(spreadCol(ConfigCol)+"1","Potential " +str([key for key in segDict.keys() if segDict.keys().index(key) == i-1])+ " Values")
    ConfigSheet.write(spreadCol(ConfigCol+segs)+"1",str([key for key in segDict.keys() if segDict.keys().index(key) == i-1])+" Enabled?")
    ConfigSheet.write(spreadCol(ConfigCol+segs+1)+"1","Enabled " +str([key for key in segDict.keys() if segDict.keys().index(key) == i-1]) +" Values")
    ConfigCol += 1


# Write the config sheet
colIterator = 1

for eachKey,eachItem in segDict.iteritems():
    rowIterator = 2
    print eachKey,eachItem
    if options.verbose:
        print "Writing Config information for "+eachKey+"..."
    for eachValue in eachItem:
        if options.verbose:
            print "Possible "+eachKey+" values: "+eachValue
        ConfigSheet.write(spreadCol(colIterator)+str(rowIterator),eachValue)
        rowIterator+=1
    colIterator += 1
        
# close simulator    
simulator.close()
exit("done")


#simulator.close()

#Remove files
#subprocess.call(['rm','turfquestions.txt'],shell=True)
#subprocess.call(['rm','turf-data.txt'],shell=True)



##FIRST 
# =IF(AND(C$2,COUNTIF(Config!$A:$A,C3)),1,0)
##First Row second col
# =IF(AND(D$2,COUNTIF(Config!$A:$A,D3)),1,0)
# Second row first col
#=IF(AND(C$2,COUNTIF(Config!$A:$A,C4)),1,0)
# First row percentage
#=IF(COUNTIF(Config!$D:$D,$B3),IF(SUM(AA3:AW3),1,0),"")
#Second row percentage
# =IF(COUNTIF(Config!$D:$D,$B4),IF(SUM(AA4:AW4),1,0),"")
