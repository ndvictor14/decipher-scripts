#!/usr/bin/perl
# Author: Victor Hernandez (vhernandez@decipherinc.com)
# Date: June 26, 2014
# Description: This file is going to do MAGIC and make the turf EVERYTHING for you :)

use strict;
use Getopt::Long();
use autodie; #Sad Face
use Spreadsheet::WriteExcel; #Excel for TURF Simulator
use Scalar::Util qw(looks_like_number); # This is used to verify numeric input for some options

#sub routine for printing usage options
sub usage {
    my $message = $_[0];
    if (defined $message && length $message) {
      $message .= "\n"
	  unless $message =~ /\n$/;
    }

    my $command = $0;
    $command =~ s#^.*/##;

   print STDERR (
      $message,
      "usage: $command -q questionLabel [-v] [-oQ] [-oD] [-rc #ofRowCombinations (default is 5)] [-c #numberOfCombination attempts (default is 100)] [-fQ turfQuestionRows.txt] [-fD turf-data.txt] [-fX spreadSheetFile.xls]\n" .
      "       ...\n" .
      "       ...\n" .
      "       ...\n"
       );

   die("\n")
}

# Option Variables
my $help;
my $questionLabel;
my $verbose;
my $questionFile;
my $dataFile;
my $rowCombinations;
my $questionFileName;
my $dataFileName;
my $xlName;
my $combinations;

#Handle options
Getopt::Long::GetOptions('h' => \$help, 'help' => \$help, 'v' => \$verbose, 'verbose' => \$verbose, 'q=s' => \$questionLabel, 'rc=i' => \$rowCombinations, 'c=i' => \$combinations, 'oQ' => \$questionFile, 'oD' => \$dataFile, 'fQ=s' => \$questionFileName, 'fD=s' => \$dataFileName, 'fX=s' => \$xlName);

#Set Defaults if needed
if ($help){
    usage();
}

if (defined $rowCombinations){
    if (looks_like_number($rowCombinations)){
	if ($verbose){
	    print "Row Combinations to be generated: $rowCombinations\n";
	}
    }
    else{
	die("The -rc option takes a numeric value!\n");
    }
}
else{
    $rowCombinations = 5;
}

if (!defined $questionFile){
    $questionFile = "turfquestions.txt";
}

if (defined $combinations){
    if (looks_like_number($combinations)){
	if ($verbose){
	    print "Combinations to be generated: $combinations\n";
	}
    }
    else{
	die("The -c option takes a numeric value!\n");
    }
}
else{
    $combinations = 100;
}


usage("Please specify the TURF question label with the -q option")
    unless defined($questionLabel);

#Start doing work 
if ($verbose){
    print "Thank you for using this AWESOME TURFulator!\n";
}

#Open the survey file and look for the question label provided
if ($verbose){
    print "Opening survey.xml.\n";
}


#Let's take a look at the survey file
my $file = "survey.xml"; 
open(XML, $file);
my @xml = <XML>;
close(XML);
my $offset = 0;

if ($verbose){
    print "Searching for question with label $questionLabel...\n";
}

my $rowCount;
my @rowArray = @xml;

# Count the number of rows in the turf question
while (my $line = shift @rowArray){
# get rid of the questions we dont care about at the beginning of the array
    shift @rowArray if $line =~ m/label="\Q$questionLabel"/;
# find the TURF question and count the number of rows
    if ($line =~ m/label="\Q$questionLabel"/){
	if ($verbose){
	    print "Determining question rows...\n";
	}
	while ($line = shift @rowArray){
	    if ($line =~ m/<row/){
		$rowCount++;
	    }

	    last if $line =~ m/<suspend.*/;
	}
    }
}

# Let's make sure a turf questions hasn't already been added....
foreach my $line (@xml){
    my $tquestion = $questionLabel."_CB";
    if ($line =~ m/label="\Q$tquestion"/){
	print "TURF Question has already been added...\n";
    }
}

#Create the new question for turf-analyze
foreach my $line (@xml){
    if ($line =~ m/label="\Q$questionLabel"/){
	if ($verbose){
	    print "Question Found!\n";
	}
	my @array = @xml;        # Copy
	while (my $token = shift @array) {
	    shift @array if $token !~ m/<suspend\/>/; # consumes the next element
	    $offset++;
	    if ($token =~ m/<suspend\/>/){
		$offset++;
		if ($verbose){
		    print "Generating New Question ".$questionLabel."_CB\n";
		    print "My offset: $offset\n";
		}
		last;   
		splice @xml, $offset, 0,  "<checkbox label=\"".$questionLabel."_CB\" atleast=\"1\" title=\"Checkbox $questionLabel\" onLoad=\"copy('$questionLabel', rows=True)\" virtual=\"setIfEqual($questionLabel ,0)\" where=\"none,notdp\"/>\n\n";
	    }
	}
    }
    $offset++;
}

# Actually Transfer Questions to file
open (OUT, ">", "trial.xml");
foreach my $line (@xml){
    print OUT $line;
}
if ($verbose){
    print "Question successfully added.\n";
}


# Run the turf script to generate rows
my @combos;
my $i = 0;
while ($rowCombinations > 0){
    $combos[$i] = ($i + 1)." ";
    $i++;
    $rowCombinations--;
}

open(my $TQ, ">", $questionFile);

# run turf-analyze
my $ta = "python /home/jaminb/v2/temp/briang/python/combo.py";
if ($verbose){
    print "Executing turf-analyze...\n";
    print "for x in ", @combos, "; do turf-analyze . ", $questionLabel."_CB all ", $combinations, " \$x | turf-format; done > ", $questionFile."\n";
}

system("for x in @combos; do turf-analyze . ".$questionLabel."_CB all $combinations \$x | $ta; done > $questionFile");

close ($TQ);

if ($verbose){
    print "\n\nTURF rows successfully generated.\n";
}

# Now let's create an ORDERED row turf question
open(QFile, "<", $questionFile); # Open our new question file for reading;
print "Processing file\n";
my @fileArray = <QFile>; #Copy file contents to array
close(QFile);
shift @fileArray;
 #Individual Rows on TURF question that will be used to created the ordered version
my @singleRows;
$i = 0; #Reset my dummy variable

foreach my $line(@fileArray){
    if ($line =~ m/<!--.*-->/){
	last;
    }
    push(@singleRows,$line);
}

# Create the ordered version
my @orderedRows; 
my $numRows = scalar(@singleRows);
print "NUM ROWS: $numRows\n\n";

while ($i <= $numRows){
    foreach my $row (@singleRows){
	if ($row =~ m/.*\Qlabel='$i'/){
	    $orderedRows[$i] = $row;	    
	}
    }
    if ($i == $numRows){
	$orderedRows[$i] = $fileArray[$i-1];
    }
    $i++;
}

if ($verbose){
    print "Ordered rows successfully generated...\n";
}
my $numberOfRows = scalar(@orderedRows); #This is used to determine the number of rows for steps in the simulator

unshift(@orderedRows,"<checkbox label=\"$questionLabel"."_ORDERED_TURF\" atleast=\"1\" title=\"Turf Combination of 1 Item - $questionLabel\" virtual=\"combo($questionLabel, 0)\">"."\n");
push(@orderedRows,"</checkbox>\n\n");

foreach my $line (@xml){
    print OUT $line;
}

#Now we create FULL questions out of the rows created
$file = "turfquestions.txt";
open(XML, $file);
@xml = <XML>;
close(XML);
$offset = 0; #Reset my counter 
my $tqText;
foreach my $line (@xml){
    # If line is a descriptor replace with actual question tag
    if ($line =~ m/<!-- combo count:/){
	# This is the combo count number 
	$line =~ tr/0-9//cd;
	if ($verbose){
	    print "Adding Question for $line Combinations...\n";
	}
#Let's make it be grammatically correct

	if ($line =~ m/1/){
	    $tqText = "<checkbox label=$questionLabel"."_".$line."_TURF\" atleast=\"1\" title=\"Turf Combination of $line Item - $questionLabel\" virtual=\"combo($questionLabel ,0)\">"."\n";
	}
	else {
	       $tqText = "</checkbox>\n\n<checkbox label=\"$questionLabel"."_$line"."_TURF\" atleast=\"1\" title=\"Turf Combination of $line Items - $questionLabel\" virtual=\"combo($questionLabel ,0)\">\n";
	}
	$line =~ s/.*/$tqText/;
	$xml[$offset] = $line;

    }
    $offset++;
}

$xml[scalar(@xml)+1] = "</checkbox>\n";

unshift(@xml,@orderedRows);

open(my $myTest, ">", "testFinal.txt");


#Now we add the new generated questions to the survey file

if ($verbose){
    print "Adding Turf Questions.....\n\n";
}

#add the questions into the file 
$file = "trial.xml"; 
open(XML, $file);
my @survey = <XML>;
close(XML);
$offset=0;

my $tQuestion = $questionLabel."_CB";

#Find the initial question & add TURF QUESTIONS
foreach my $line (@survey){
    if ($line =~ m/\Q$tQuestion/){
	#Add the TURF Questions
	splice @survey,$offset+1,0,@xml;
	last;
    }
    $offset++;
}

#Print the final product for the survey :)
print $myTest @survey;



#Generate Data to be used later in excel spreadsheet
#system("generate . qualified tab uuid list $(seq 1 18 | sed 's/^/".$questionLabel."r/' | tr '\n' ' ') > turf-data.txt");

# Create a new workbook called simple.xls and add a worksheet.
my $workbook  = Spreadsheet::WriteExcel->new('simulator.xls');
my $simulator = $workbook->add_worksheet();
my $data = $workbook->add_worksheet();
my $Config = $workbook->add_worksheet();

#Set up the config sheet

#Enabled Values
$Config->write(0,0,"Enabled Values");
$Config->write(1,0,5);

#Segments
$Config->write(0,1,"Potential \"Segment\" Values");

#Enabled
$Config->write(0,2,"Enabled");
$Config->write(1,2,"False");

#Enabled Values
$Config->write(0,3,"Enabled Segment Values");
$Config->write('D2', 'IF(C2=TRUE,1,)');


