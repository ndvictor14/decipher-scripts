#!/usr/bin/python

##############################################################
# Created By : Victor Hernandez (vhernandez@decipherinc.com) #
##############################################################

from __future__ import print_function #time travelling is a thing in python you know
import os.path
import subprocess
import re
import sys
from optparse import OptionParser


    # Check if filename is taken
def noFileConflict(searchFile):
    if not os.path.isfile(searchFile):
        return True
    else:
        return False

# Check if user wants to overwrite or rename output files
def overwrite(searchFile):
    print(searchFile +  " already exists in this directory!")
    answer = raw_input("Would you like to overwrite? (Y|N): ")
    
    # User wants to overwrite file
    if re.match(r"y|Y|yes|YES",answer):
        return searchFile
    # User wants to provide alternative file name
    else:
        # Make sure the new file isn't also taken
        searchFile = raw_input("Enter new filename: ")
        if not noFileConflict(searchFile):
            overwrite(searchFile)
        return searchFile
    

# Script flags
parser = OptionParser()
parser.add_option("-e", "--email", dest="email",
                  help="Send file to this email")
parser.add_option("-s","--status", dest="status",
                  help="Specify only select statuses (comma delimited) to output files for [valid values: ok,bb (bouncebacks),oo (optouts)]")
parser.add_option("-f","--files", dest="filenames",
                  help="Only use these files")
parser.add_option("-c", "--concactenate", dest="concat", default=False, action="store_true",
                  help="Use the log files and also get the additional information for these respondents from the list files. By default it uses the list files, you can pass an additional file using the -a flag")
parser.add_option("-a","--additionalFile",dest="catfile",
                  help="Additional file used for appending information from respondents in the logs")

(options,args) = parser.parse_args()

if options.status:
    # Remove duplicates from the options passed in ( just in case :) )
    statuses = list(set(options.status.split(',')))

    # Check that the statuses passed are all valid
    for status in statuses:
        if status not in ['ok','bb','oo']:
            exit(status + " is not a valid status value. Use sendstatus -h for more help.")

outputFileNames=['optouts.txt','oks.txt','bouncebacks.txt']

# Set output file names (ie check overwriting)
for filename in outputFileNames:
    if not noFileConflict(filename):
        outputFileNames[outputFileNames.index(filename)] = overwrite(filename)

# Create output files
ooFile = open(outputFileNames[0], "w")
okFile = open(outputFileNames[1],"w")
bbFile = open(outputFileNames[2],"w")

# If not logs were specified, create a master log based off initial log files
if not options.filenames:
  subprocess.call('cat list*.txt.log > sendStatusLogs.txt', shell=True)

# Clean up the log file by removing the send commands,date information and making it tab delimited
subprocess.call('sed -i "/#.*/d" sendStatusLogs.txt', shell=True)
subprocess.call('sed -i "s/.*:.. //" sendStatusLogs.txt', shell=True)
subprocess.call('sed -i -e "s/ (/\t(/" sendStatusLogs.txt',shell=True)

#Add the headers to the cleaned up file
subprocess.call('echo "email\tstatus"|cat - sendStatusLogs.txt > /tmp/out && mv /tmp/out sendStatusLogs.txt',shell=True)

# Handle Concat options
if options.concat:
    # If concat option and no specified file, create one from all lists
    if not options.catfile:
        subprocess.call('cat list*.txt > sendStatusList.txt',shell=True)
        options.catfile = "sendStatusList.txt"
    subprocess.call('gcat -t"email" --d %s sendStatusLogs.txt > sendStatusInfo.txt' % options.catfile, shell=True)

# Add header to output files
for i in range(0,3):
    subprocess.call('head -n 1 sendStatusInfo.txt | cat - %s > /tmp/out && mv /tmp/out %s' % (outputFileNames[i],outputFileNames[i]),shell=True)

# Break the info file down to the status files
with open("sendStatusInfo.txt") as logFile:
    # Go through each line of the master log
    for line in logFile:
        if (options.status and 'ok' in statuses) or not options.status:
            if re.match(r".*(ok)", line):
                print okFile >> line.rstrip()
        if (options.status and 'oo' in statuses) or not options.status:
            if re.match(r".*\(opt.*", line):
                print(line.rstrip(),file=ooFile)
        if (options.status and 'bb' in statuses) or not options.status:
            if re.match(r".*\(bounce.*",line):
                print(line,file=bbFile)

#close all files
ooFile.close()
okFile.close()
bbFile.close()
logFile.close()
exit("Check files")

# Convert to xlsx files
if os.path.isfile(outputFileNames[0]):
    subprocess.call('python /home/jaminb/v2/temp/victor/scripts/td2xlsx.py %s optouts.xlsx' % outputFileNames[0], shell=True)
if os.path.isfile(outputFileNames[1]):
    subprocess.call('python /home/jaminb/v2/temp/victor/scripts/td2xlsx.py %s oks.xlsx' % outputFileNames[1], shell=True)
if os.path.isfile(outputFileNames[2]):
    subprocess.call('python /home/jaminb/v2/temp/victor/scripts/td2xlsx.py %s bouncebacks.xlsx' % outputFileNames[2], shell=True)
exit() # Debugging exit



#get rid of our traces unless dirty :D
if not options.dirty:
    subprocess.call('rm sendStatusLogs.txt', shell=True)
    subprocess.call('rm sendStatusList.txt', shell=True)
    subprocess.call('rm sendStatusInfo.txt', shell=True)

#Friendly output
print("Thanks for flying Vic :)")
