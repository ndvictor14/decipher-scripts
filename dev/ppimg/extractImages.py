#!/usr/bin/python2.7

# This script will extract the images from .xls, .xlsx, .ppt, and .pptx files

import sys
import argparse
import re
from subprocess import call
import random

comments = ["Extracting", "Rescuing", "Grabbing", "Birthing", "Ressurecting"]

# Function definition for extracting from .ppt and .pptx files, clean is a boolean that will determine if the original file should be kept (clean == 0)
def extract(fileToExtract,clean):
  myComment = comments[random.randint(0,len(comments)-1)]
  newZip = str(re.sub(r'\..*','',fileToExtract))
  newZip = newZip + ".zip"

  # Handle the way the new zip file will be created
#  if clean:
#    call(["mv", fileToExtract, newZip])
#  else:
  call(["cp", fileToExtract, newZip])

  # Unzip the zip :D
  call(["unzip", newZip])

  # Dive into the directory and extract the images :D
  Pictures = call(["ls","Pictures"])


parser = argparse.ArgumentParser(description="Extract image from files.")

# Argument for files to extract from 
parser.add_argument("-f","--file", default=[], dest="files", type=str, nargs='+')

# Location to place images, if none specified will look for a static folder / create one if it doesn't exist
parser.add_argument("-d","--destination", dest="location")

# Enable verbose argument
parser.add_argument("-v","--verbose", dest="verbose", action='store_true')

# Get the arguments, args is used to access each option
args = parser.parse_args()

validExtensions = ['ppt','pptx','xls','xlsx']
filesToExtract = [] # This will hold the valid files
# Look for acceptable files based on extensions
for myfile in args.files:
  extension = re.sub(r'.*\.','',myfile)
  print "My Extension: %s and valid extensions: %s" % (extension,validExtensions)
  if extension in validExtensions:
    filesToExtract.append(myfile)
  else:
    print "Unable to extract from %s! File does not have a valid extension!\n" % myfile

if len(filesToExtract)== 0:
  exit("There are no files to extract from! Good-bye!\n\n")
elif (args.verbose):
  print "Beginning image extraction....\n"

for eachFile in filesToExtract:
  extract(eachFile,1)
