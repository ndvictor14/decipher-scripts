#!/usr/bin/python2.7

#############################################
# Send RSI send report and seed list to AM  #
#############################################


__author__ = """

      (`-.                     .-') _                _  .-')        
    _(OO  )_                  (  OO) )              ( \( -O )       
,--(_/   ,. \ ,-.-')   .-----./     '._  .-'),-----. ,------.       
\   \   /(__/ |  |OO) '  .--./|'--...__)( OO'  .-.  '|   /`. '      
 \   \ /   /  |  |  \ |  |('-.'--.  .--'/   |  | |  ||  /  | |      
  \   '   /,  |  |(_//_) |OO  )  |  |   \_) |  |\|  ||  |_.' |      
   \     /__),|  |_.'||  |`-'|   |  |     \ |  | |  ||  .  '.'      
    \   /   (_|  |  (_'  '--'\   |  |      `'  '-'  '|  |\  \       
     `-'      `--'     `-----'   `--'        `-----' `--' '--'   

"""

import os
import argparse
import subprocess
import re 
from operator import itemgetter
# Specify script options
parser = argparse.ArgumentParser(description='Send logs and reports to AM.')

# Argument for survey path
parser.add_argument('-p','--path', dest="surveyPath",help='survey path - if not specified, defaults to current location') # defaults to current path
parser.add_argument('-v','--verbose', action="store_true",help='verbose output') # defaults to current path
parser.add_argument('-f','--force', action="store_true",help='Send newest logs and reports for all lists in the mail directory.') # defaults to current path
args = parser.parse_args()

# default survey path to current directory if none specified
if not args.surveyPath:
    args.surveyPath = "." 
else:
    args.surveyPath = "/home/jaminb/v2/"+args.surveyPath
dirFiles = subprocess.check_output(['ls',args.surveyPath])

# Check that mail directory is in project
if 'mail' not in dirFiles:
    exit("Mail directory not found in survey directory!")

# If script didn't end check the files in the mail directory 
dirFiles = subprocess.check_output(['ls',args.surveyPath+"/mail"])

#regular expressions for finding file types
logs = re.compile('.*\.log')
reports = re.compile('.*\.report')
originals = re.compile('list*.*txt')
evites = re.compile('email*')


def getBase(fileName):
    """Get base name (no extension)"""
    return re.sub(r'\..*','',fileName) # files shouldn't be in formate some.more.txt. ever.
    
#arrays to hold files
results = []

# Split mail directory files accordingly - First find list files and create dictionary
if args.verbose:
    print "Files in mail directory: "
for eachFile in dirFiles.split('\n'):
    print eachFile
    if re.match(originals,eachFile) and not (re.match(logs,eachFile) or re.match(reports,eachFile)):
        theBase = getBase(eachFile)
        theD = {'listFile': theBase, 'reportFiles': [], 'logFiles': []}
        results.append(theD)


for eachFile in dirFiles.split('\n'):
    if re.match(reports,eachFile):
        base = getBase(eachFile)
        for eachListFile in results:
            if eachListFile['listFile'] == base:
                eachListFile['reportFiles'].append(eachFile)
    elif re.match(logs, eachFile):
        base = getBase(eachFile)
        for eachListFile in results:
            if eachListFile['listFile'] == base:
                eachListFile['logFiles'].append(eachFile)


print results

def findNewest(files):
    """Find files from the latest send"""
    filesWithInfo = []
    for eachFile in files:
        numbers = re.compile('rem\d+(?:\.\d+)?')
        if numbers.search(eachFile) is not None:
            filesWithInfo.append({eachFile: int(numbers.findall(eachFile)[0].strip('rem'))})
        else:
            filesWithInfo.append({eachFile: 0})
    print max(filesWithInfo[0].iteritems(), key=itemgetter(1))[0]
            

    
filesToSend = []
for eachItem in results:
    if len(eachItem['reportFiles']) > 1:
         findNewest(eachItem['reportFiles'])
         findNewest(eachItem['logFiles'])
    else:
        print 'blah'
