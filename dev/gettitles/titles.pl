#!/usr/bin/perl

use strict;
use Getopt::Long();
use autodie; #Sad Face
use Scalar::Util qw(looks_like_number); # This is used to verify numeric input for some options


my $file = "survey.xml"; 
open(XML, $file);
my @xml = <XML>;
close(XML);
open (OUT, ">", "titles.txt");
my @titles;
my @output;

while (my $line = shift @xml){
    shift @xml if $line =~ m/<title>/;
    if ($line =~ m/<title>/){
	push(@titles, $line);
    }
}


foreach my $title (@titles){
    print $title."\n";
    $title =~ s/<title>//;
    $title =~ s/<\/title>//;
    print OUT $title;
}


