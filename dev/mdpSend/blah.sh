#!/bin/bash

#set -e
#set -x

mag_list=( 'ab' 'bh' 'fc' 'ft' 'mo' 'pa' 'th' 'ar' 'ew' 'ff' 'mw' 'rr' )
initial_headers=( 'email' 'name' 'sp' 'source' 'keycode' 'gr' )
final_headers=( 'email' 'name' 'sp' 'source' 'keycode' 'gr' 'cmpn' )






create()
{
   echo -en "What is the campaign date? e.g. ${ESC}${BYELLOW}20140604${ESC}${NORMAL}: "
   read cmpn

   echo "Preparing list files and copying to rsi10035/mail"
   for i in ${mag_list[@]}
   do
     #add cmpn column to list file
     echo "Adding cmpn column to ${i}"
     coladd -v list-${i}.txt:cmpn=${cmpn} > list-${i}.cmpn.txt
  
     #update all the header files with the correct cmpn date
     echo "Adding header to ${i}"
     sed "s/XXXXXXXX/$cmpn/" /home/jaminb/beta/mailing/pm/mdp/evites/headers-$i.txt > headers-$i.txt

     #put the seeds from the header file after the second line of each list file
     echo "Adding client seeds to ${i}"
     echo -e "1r headers-$i.txt\nw" | ed list-$i.cmpn.txt > /dev/null

     #overwrite the original list files with the updated files
     mv list-$i.cmpn.txt list-$i.$cmpn.txt
     rm list-$i.txt
     mv list-$i.$cmpn.txt list-$i.txt

     #remove the header files from the directory (not the main headers in the evites/ folder)
     rm headers-$i.txt

     #Move the list with the cmpn date into pm/mdp so the campaigns tab populates with the list data
     #mv list-$i.$cmpn.txt /home/jaminb/v2/pm/mdp/mail/

   done


   #get only the source column for all files and append to the rsi0035/invited.txt
#   echo "Creating invited.txt file to add to rsi10035"
#   gcat -h"source" list-*.txt > invited.txt

   #append the sources into rsi10035 invited.txt
#   echo "Adding the source variables to the survey's invited.txt using: cat invited.txt >> /home/jaminb/v2/rsi/rsi10035/invited.txt"
#   cat invited.txt >> /home/jaminb/v2/rsi/rsi10035/invited.txt
#   echo "Add this as a new row in vcmpn in ~v2/rsi/rsi10035/survey.xml:"
#   echo -e '\E[37;44m'"\033[1m  <row label='${cmpn}'>${cmpn}</row>\033[0m"
   echo "Add the new cmpn value ${cmpn} to the var tag in the samplesource in pm/mdp"

}


schedule()
{
   echo "Scheduling the send will happen here...eventually"
   echo -en "What is the first time/day the evites start going out? (6am 07/11/2014 for example): "
   read firstinitial
   echo -en "What is the second time/day the evites start going out? (6am 07/12/2014 for example): "
   read secondinitial
   echo -en "What is the third time/day the evites start going out? (6am 07/13/2014 for example): "
   read thirdinitial
   echo -en "What is the first time/day the REMINDER evites will start going out? (6am 07/14/2014 for example): "
   read firstreminder
   echo -en "What is the second time/day the REMINDER evites will start going out? (6am 07/15/2014 for example): "
   read secondreminder
   echo -en "What is the third time/day the REMINDER evites will start going out? (6am 07/16/2014 for example): "
   read thirdreminder
   echo -en "How many evites will be sent out per day? (From Baljit's email): "
   read num
   echo -en "How many evites per hour? (From Baljit's email): "
   read rate

   echo "mdp-bulk-email -n${num} --rate=${rate} send" | at "${firstinitial}"
   echo "mdp-bulk-email -n${num} --rate=${rate} send" | at "${secondinitial}"
   echo "mdp-bulk-email -n${num} --rate=${rate} send" | at "${thirdinitial}"

   echo "mdp-bulk-email -n${num} --rate=${rate} -R1 --reminder-for=1 send" | at "${firstreminder}"
   echo "mdp-bulk-email -n${num} --rate=${rate} -R1 --reminder-for=2 send" | at "${secondreminder}"
   echo "mdp-bulk-email -n${num} --rate=${rate} -R1 --reminder-for=3 send" | at "${thirdreminder}"


#TODO: Ask how many days it will be spread out across and loop through to schedule that many times
#TODO: Automatically figure out the 3 days in a row based on the initial day and the reminders as well
   
#We have a new way of scheduling the sends / reminders in that we’re doing X emails / hour. See below for the initial send code:
#at “6am 11/26/13”
#mdp-bulk-email -nXXX --rate=XXX send
#ctrl+d
#And this for reminders:
#at “6am 11/30/13”
#mdp-bulk-email -nXXX --rate=XXX -Rxxx --reminder-for=XXX send
#ctrl+d  

#ALSO maybe open the survey.xml file for rsi10035 and pm/mdp?


#Thinking about splitting this into a setup/send script like maps.  Basically, you would run the script and it would ask:
#(C)reate or (S)end?
#If you create it would set up the list files and then have you add the correct cmpn values
#Send would:
#1. Check to make sure all the sources exist in rsi10035
#2. Check to make sure the cmpn value exists in vcmpn in rsi10035
#3. Check to make sure the cmpn value exists in the samplesource of pm/mdp
#4. Make sure the headers are all correct/match
#5. Schedule the initial sends with echo "mdp-bulk-email -nXXX --rate=XXX send" | at "6:00am MM/DD/YY"
#6. Schedule the reminders with echo "mdp-bulk-email -nXXX --rate=XXX -Rxxx --reminder-for=XXX send" | at "6am MM/DD/YY"
}


echo -en "(${ESC}${BYELLOW}C${ESC}${NORMAL})reate Files or (${ESC}${BYELLOW}S${ESC}${NORMAL})chedule send: "
read opt1

opt1=`echo $opt1 | awk '{print tolower($opt1)}'`

if [[ "$opt1" == "c" || "$opt1" == "create" ]]
then
    create  
else    
    if [[ "$opt1" == "s" || "$opt1" == "schedule" ]]
    then
        schedule
    fi
fi

