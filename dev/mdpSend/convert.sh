#!/bin/bash

echo -en "What is the first time/day the evites start going out? (6am 07/11/2014 for example): "
read firstinitial
echo $firstinitial

if [ ! "$firstinitial" ]
then
    echo "Invalid number of parameters. Must specify both time and date in [time][meridean] [date] format."
    exit 1
fi

desiredTime=${firstinitial% *}
let numerical=`expr "$desiredTime" : '\([0-9][0-9]*\)'`
meridean=`expr "$desiredTime" : '\([a-z][A-Z]*\)'`


echo $numerical
