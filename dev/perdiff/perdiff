.\" Manpage for perdiff.
.\" Contact vhernandez@decipherinc.com to correct errors or typos.
.TH man 8 "6 January 2015" "1.0" "perdiff man page"
.SH NAME
perdiff \- calculate percentage change from versioned files.
.SH SYNOPSIS
perdiff [-p path] [-f file (defaults to survey.xml if not specified)] [-d startDate:endDate (defaults to yesterday and today if not specified)]
.SH DESCRIPTION
perdiff allows you to specify a file and determine how much the file has changed in the specified date range. It outputs a file with all relevant information.

.SH OPTIONS
The -p option is optional. It takes a full path as the argument. If no path is specified it defaults to the user's current path. The file option is also optional. This is the file that will be checked. If no file name is specified it defaults to the "survey.xml" file. The -d option takes the date range in mm/dd/yy:mm/dd/yy format. If this option is not specified, the script will compare yesterday's version of the file to today's version. The -t option is added in case a more precise version is desired. The -t option takes a specified time. This option can be used in union with the -d option. If the -d option is ommitted and the -t option is passed, the script will use the first instance of the time. For example if you used -t "10:00AM" at 12:00PM today, it would look at the version that existed at 10:00 this morning, but if you also pass -d yesterday:today it would look at the version that existed at 10:00AM yesterday.
.SH SEE ALSO

.SH BUGS
No known bugs.
Please report any bugs to: vhernandez@decipherinc.com
.SH AUTHOR
Victor Hernandez (vhernandez@decipherinc.com)
