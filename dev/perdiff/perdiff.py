#!/usr/bin/env hpython

__author__ = """
.------..------..------..------..------..------.
|V.--. ||I.--. ||C.--. ||T.--. ||O.--. ||R.--. |
| :(): || (\/) || :/\: || :/\: || :/\: || :(): |
| ()() || :\/: || :\/: || (__) || :\/: || ()() |
| '--'V|| '--'I|| '--'C|| '--'T|| '--'O|| '--'R|
`------'`------'`------'`------'`------'`------'
"""

import pkg_resources, hstub
from hermes import Survey, misc
from util import fatal
import argparse
import os
import sys
import subprocess

# Ensure vc is loaded or output friendly error
try:
    import vc
except:
    exit("Unable to import version control module.")



arguments = argparse.ArgumentParser(description='Calculate survey change percentage.', add_help=False)
arguments.add_argument('-s','--surveyPath')
arguments.add_argument('-d','--dateRange')
arguments.add_argument('-v','--verbose')

args = arguments.parse_args()

pkg_resources,hstub

basePath = "/home/jaminb/v2/"

# Default to current path if none specified
if not args.surveyPath:
    surveyPath = basePath + misc.expandSurveyPath('.')
else:
    surveyPath = basePath + args.surveyPath

#Some verbosity
if args.verbose:
     print "Attempting to open: %s" % surveyPath

# Load survey object
#surveyName = misc.expandSurveyPath(surveyPath)
print surveyPath
theSurvey = Survey.loadNoException(surveyPath)
if theSurvey == None:
    exit("Unable to open specified survey: %s" % surveyPath)

