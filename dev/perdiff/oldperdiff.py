#!/usr/bin/python

###############################################################################
#          Created By: Victor Hernandez (vhernandez@decipherinc.com)          #
###############################################################################

import os
import subprocess
from optparse import OptionParser
import datetime

      # Define some keywords
today = datetime.datetime.now()
print "Python's today: %s " % today


      # First check that a survey.xml files exists in the specified directory
parser = OptionParser()
parser.add_option('-p','--path', dest="path",help="Survey Path")
parser.add_option('-d','--date',dest="dateRange", help="Date range")
parser.add_option('-f','--file',dest="diffFile", help="BETA: File to calculate percentage difference")
parser.add_option('-t','--time',dest="timeRange", help="Time range")
(options,args) = parser.parse_args()

      # If not path is given use the current path as the survey path
if not options.path:
    pwdOut = subprocess.Popen('pwd', stdout=subprocess.PIPE)
    path = pwdOut.stdout.read()

      # If no file is specified assume it's survey.xml
if not options.diffFile:
    diffFile = "survey.xml"

      # Make sure the file exists in the current path
if not os.path.isfile("%s/%s" % (path.rstrip(),diffFile)):
    print "survey.xml does not appear to exist at %s" % path
else:
    diffFile = path.rstrip() + "/survey.xml"

# check if data range is specified, if none given we're just going to assume it's from the last two versions
#if not options.dateRange and not options.timeRange:
#    dateRange 



