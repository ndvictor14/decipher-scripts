#!/usr/bin/env hpython

__author__ = """
      (`-')  _               (`-')                   (`-')  
     _(OO ) (_)     _        ( OO).->       .->   <-.(OO )  
,--.(_/,-.\ ,-(`-') \-,-----./    '._  (`-')----. ,------,) 
\   \ / (_/ | ( OO)  |  .--./|'--...__)( OO).-.  '|   /`. ' 
 \   /   /  |  |  ) /_) (`-')`--.  .--'( _) | |  ||  |_.' | 
_ \     /_)(|  |_/  ||  |OO )   |  |    \|  |)|  ||  .   .' 
\-'\   /    |  |'->(_'  '--'\   |  |     '  '-'  '|  |\  \  
    `-'     `--'      `-----'   `--'      `-----' `--' '--' 
"""

import pkg_resources, hstub
from hermes import Survey, misc
from util import fatal
import argparse
import os
import sys
import subprocess
import xlwt
import re
from xml.dom import minidom
import xml.etree.ElementTree as ET
from HTMLParser import HTMLParser

# Create the survey Object --- Since making a quota should be called within the appropriate directory to avoid incorrectly overwriting quotas for a different project, specified paths will not be allowed 
surveyName = "."

surveyName = misc.expandSurveyPath(surveyName)
theSurvey = Survey.loadNoException(surveyName)
if theSurvey == None:
    exit("Unable to open specified survey: %s" % surveyName)


# function to make friendly markers 
def safeString(theString):
    newString = re.sub(' |-','_',theString)
    return newString


# Function for prompting for quota title
def promptTitle(theCurrentTitle):
    print "Current table title: %s. Input new title or press enter to leave as is." % theCurrentTitle
    if args.skip:
        return theCurrentTitle
    newTitle = raw_input("Table title: ")
    if newTitle == "":
        newTitle = theCurrentTitle
    return newTitle


# ASK ERWIN OR SOMEONE WHETHER THERE IS A METHOD FOR THIS IN SURVEY.PY --- GET 'VALUES' ATTRIBUTE OF SAMPLESOURCE VAR
def findExtras(theExtra):
    doc = getXML()
    xmldoc = ET.fromstring(doc)
    SS = xmldoc.findall('samplesources')
    for SSEl in SS:
        for SEl in SSEl.findall('samplesource'):
            for El in SEl.findall('var'):
                if El.get('name') == theExtra:
                    return El.get('values').split(',')

    return []


# Stuff for cleaning up alts
class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()


def checkOutEl(mySurvey,culprit):
     El = next((x for x in mySurvey.root.qElements if x.label == culprit), None)
     return El

# Check if argument is extra variable (as opposed to Question element) and return it if it exists
def checkExtra(theExtra):
    if len(findExtras(theExtra)) == 0:
        exit("Unable to find %s!" % theExtra)
    else:
        return findExtras(theExtra)

def getParentInfo(theArray, targeted, theKey):
    theParents = []
    for eachItem in theArray:
        if eachItem['parent'] == targeted:
            theParents.append(eachItem[theKey])
    return theParents
    


def getXML():
    f = open(surveyName+'/survey.xml', 'r')
    doc = f.read()
    f.close()
    doc = re.sub(r':([^ \s<])',r'\1',doc)
    return doc




# Specify script options
parser = argparse.ArgumentParser(description='Make a quota sheet from the command line.', add_help=False)
parser.add_argument('-l','--limits',help='Comma delimited list of sample quota limits.')
parser.add_argument('-v','--verbose',action="store_true", help="Verbose output.")
parser.add_argument('-h','--help',action="store_true", help="Open manpage.")
parser.add_argument('-q','--questions',nargs="+", help="Questions to add quota limits to. Space delimited. To specify limits/row: Q1:inf,inf,20 will set Q1r1 & Q1r2 limits to inf and Q1r3 limits to 20. If you want to set all row limits to inf only specify question label.")
parser.add_argument('-e','--extra',nargs="+", help="Extra variables from sample sources to track in the quotas. Space delimited. To specify limits/variable values ThisIsMyVar:inf,inf,20 will set the variables first value to inf, the second value to inf and the third value to 20. If you want to set all row limits to inf only specify the variable label. You can specify alt labels for the extra variale's values by using another ':' --- var:limits:\"This is my first alt,Now this is my second alt\".")
parser.add_argument('-n','--nest',  help="Nest quotas. verticalQuestionLabel,verticalQuestionLabel/horizontalQuestionLabel_SecondNestedQuota,nextVertical/BlahHorizontal_etc. Will always default to inf limits. NOTE: USE 'SS' FOR SAMPLESOURCES! ONLY SUPPORTS 2 VERTICAL NESTS!")
parser.add_argument('-s','--skip',action="store_true", help="Don't prompt for table titles.")
args = parser.parse_args()

if args.help:
    subprocess.call(['man','/home/jaminb/v2/temp/victor/scripts/dev/makeQuota2/./makeQuota2'])
    exit()


pkg_resources,hstub




# Check Survey States
if theSurvey.root.state.closed:
    exit("Survey state is closed. If you really want to make changes to this, set it to testing.")
elif theSurvey.root.state.live:
    exit("Survey is LIVE! I will not let you corrupt data. If you're in a temp set it to testing!")

# Check quota existance
if os.path.isfile('/home/jaminb/v2/'+theSurvey.path+'/quota.xls'):
    print "quota.xls found in current directory!\n"
    if not re.match(r'y|yes|Yes|YES|Y',raw_input("Are you sure you want to continue and overwrite? [Y/N] ")):
        exit("Aborted. Thanks for flying Vic.")

myDir = "/home/jaminb/v2/"+theSurvey.path

quotaWorkbook = xlwt.Workbook()
definesSheet = quotaWorkbook.add_sheet('Defines')
quotasSheet = quotaWorkbook.add_sheet('Quotas')



# Grab samplesource information
myss = theSurvey.root.samplesources.children
allSS = []
ssInfo = {}
for eachSS in myss:
    ssMarkerLabel = safeString(eachSS.title)
    ssInfo = {'title': eachSS.title, 'lvalue': eachSS.list, 'limit': 'inf', 'markerlabel': ssMarkerLabel}
    allSS.append(ssInfo)

if args.verbose:
    print "Found these samplesources: "
    for eachSS in allSS:
        print " " + eachSS['title'] + " "

# Update limits to those provided
if args.limits:
    allLimits = args.limits.split(',')
    for ss in allSS:
        if ( len(allLimits) < len(allSS) ) and ( allSS.index(ss) == len(allLimits) ):
            print "%s's limit's defaulting to inf." % ss['title']
        else:
            ss['limit'] = allLimits[allSS.index(ss)]

else:
    print "Sample source limits will default to infinity."






# Handle question limits
qInfo = {}
allQs = []

if args.questions:
    theQuestions = args.questions

    # Go through the specified questions
    for eachQ in theQuestions:

        # Check if row limits specified
        if ":" in eachQ:
            eachQLabel  = eachQ.split(':')[0]
            eachQLimits = eachQ.split(':')[1]

            if len(eachQLimits.split(',')) >= 1 and eachQLimits != '':
                limitsSpecified = True
            else:
                limitsSpecified = False

        else:
            eachQLabel = eachQ
            limitsSpecified = False

        # Make sure question element exists in the survey
        qEl = checkOutEl(theSurvey, eachQLabel)

        if qEl == None:
            exit("Question %s not found!" % eachQLabel)
        originalTitle = ""
        # Prompt for quota table title if necessary
        if not args.skip and qEl.title != originalTitle:
            tableTitle = promptTitle(qEl.title)
            originalTitle = strip_tags(qEl.title)
        else:
            tableTitle = strip_tags(qEl.title)

        # The question element is found, if limits specified ensure there are that many rows in the question
        if limitsSpecified:
            if len(eachQLimits.split(',')) != len(qEl.rows):
                exit("You specified %s limits for %s but %s only has %s rows." % (len(eachQLimits.split(',')),qEl.label,qEl.label,len(qEl.rows)))
        

            
            # Apply limits as necessary 
            QLimits = eachQLimits.split(',')
            for x in range(0,len(QLimits)):
                qInfo = {'qlabel': eachQLabel, 'altLabel': strip_tags(qEl.rows[x].cdata), 'qlimits': str(QLimits[x]),'markerdefinition': eachQLabel+"."+qEl.rows[x].label, 'markerlabel': eachQLabel+"_"+qEl.rows[x].label, 'title': tableTitle}
                allQs.append(qInfo)
        else:
            for eachRow in qEl.rows:
                qInfo = {'qlabel': eachQLabel, 'altLabel': strip_tags(eachRow.cdata), 'qlimits': 'inf','markerdefinition': eachQLabel+"."+eachRow.label, 'markerlabel':eachQLabel+"_"+str(qEl.rows[qEl.rows.index(eachRow)].label), 'title': tableTitle}
                allQs.append(qInfo)


# Handle extra variables
if args.extra:
    allExtras = []
    limits = 'inf'
    for x in args.extra:
        promptedForTitle = False
        if ":" in x:
            splits = True
            variable = x.split(':')[0]
            if x.split(':')[1] != '':
                limits = x.split(':')[1]

            limitsSpecified = False
            # Check if limits were specified to determine course of action
            if len(limits.split(',')) > 1:
                limitsSpecified = True

            # If limits were specified - 1 limit sets that limit to all but if more than one and the number doesn't match the number of possible values this is a syntax error
            if limitsSpecified and len(limits.split(',')) != len(findExtras(variable)):
                exit("You specified %d limits but there are %d valid values. Check your syntax." % (len(limits.split(',')), len(findExtras(variable))))
            specifiedAlts = x.split(':')[2].split(',')
        else:
            variable = x
            specifiedAlts = x
            
        
        # Make sure the variable exists
        myExtras = checkExtra(variable)
        for eachValue in myExtras:
            # This variables quota informaion
            thisDefinition = variable+"=='"+str(eachValue)+"'"
            thisMarker = variable+"_"+str(eachValue)
            try:
                thisAlt = specifiedAlts[findExtras(variable).index(eachValue)]
            except IndexError:
                thisAlt = eachValue
            # Some verbosity
            if args.verbose:
                print "Marker: %s, Definition: %s, Alt: %s" % (thisMarker,thisDefinition,thisAlt)


            # Prompt for quota table title if necessary
            tableTitle = "Split by " + variable
            if not (args.skip or promptedForTitle):
                tableTitle = promptTitle(tableTitle)
                promptedForTitle = True

            # Dict based on limit specifications
            if not limitsSpecified:
                qInfo = {'qlabel': variable, 'altLabel': thisAlt, 'qlimits': str(limits),'markerdefinition': thisDefinition, 'markerlabel': thisMarker, 'title': tableTitle}
                if args.verbose:
                    print "Created %s" % qInfo

                allQs.append(qInfo)

                # Limits specified - add appropriately 
            else:
                for eachLimit in limits:
                    qInfo = {'qlabel': variable, 'altLabel': thisAlt, 'qlimits': eachLimit,'markerdefinition': thisDefinition, 'markerlabel': thisMarker, 'title': tableTitle}
                    if args.verbose:
                        print "Created %s" % qInfo
                    allQs.append(qInfo)


# Handle nested
if args.nest:
    nestedInfo = []
    limits = 'inf'
    nestedSheet = quotaWorkbook.add_sheet('Nested')
    nests = args.nest.split('_')
    curRow = 0

    for eachNest in nests:
        curCol = 0 # Reset col for each base marker

        # Prompt for a table title
        tableTitle = promptTitle(eachNest)

        # Split up nets based on the axis they will be on
        YAxis = eachNest.split('/')[0]
        XAxis = eachNest.split('/')[1]
        allY = YAxis.split(',')
        if len(allY) > 2:
            exit("Sorry, I only support 2 Y axis nests.")

        # If SS is one of the nests, add to the nested info
        if 'SS' in allY or 'SS' in XAxis:
            for eachSS in myss:
                ssMarker = safeString(eachSS.title)
                if "list=='"+eachSS.list+"'" not in [D['definition'] for D in nestedInfo]:
                    thisInfo = {'marker': ssMarker, 'parent': 'SS', 'definition': "list=='"+eachSS.list+"'", 'alt': eachSS.title  + " Sample"}
                    nestedInfo.append(thisInfo)

        # Check if markers made for X-axis items or make them
        if XAxis not in [Q['qlabel'] for Q in allQs] and XAxis != 'SS':
            extraVar = False
            thisEl = checkOutEl(theSurvey,XAxis)
            if thisEl == None:
                thisEl = checkExtra(XAxis)
                extraVar = True
            if extraVar:
                for eachValue in thisEl:
                    marker = XAxis+"_"+eachValue
                    definition = XAxis + "=='" + eachValue +"'"
                    thisAlt = XAxis + " = " + eachValue
                    if definition not in [D['definition'] for D in nestedInfo]:
                        thisInfo = {'marker': marker, 'definition': definition, 'alt': thisAlt, 'parent': XAxis}
                        nestedInfo.append(thisInfo)
            # Question Element
            else:
                for eachRow in thisEl.rows:
                    marker = thisEl.label + "_" + eachRow.label
                    definition = thisEl.label + "." + eachRow.label
                    thisAlt = strip_tags(eachRow.cdata)
                    if definition not in [D['definition'] for D in nestedInfo]:
                        thisInfo = {'marker': marker, 'definition': definition, 'alt': thisAlt, 'parent': XAxis}
                        nestedInfo.append(thisInfo)


        # Check if marker was already made for Y-axis items
        for eachY in allY:

            if eachY not in [Q['qlabel'] for Q in allQs] and eachY != 'SS':
                # Wasn't specified --- need to make markers
                try:
                    thisEl = checkOutEl(theSurvey,eachY)
                    extraVar = False
                except ValueError:
                    thisEl = checkExtra(eachY)
                    extraVar = True
                # Extra variable
                if extraVar:
                    for eachValue in thisEl:
                        marker = eachY+"_"+eachValue
                        definition = eachY + "=='" + eachValue +"'"
                        thisAlt = eachY + " = " + eachValue
                        if definition not in [D['definition'] for D in nestedInfo]:
                            thisInfo = {'marker': marker, 'definition': definition, 'alt': thisAlt, 'parent': eachY}
                            nestedInfo.append(thisInfo)
                # Question Element
                else:
                    for eachRow in thisEl.rows:
                        marker = thisEl.label + "_" + eachRow.label
                        definition = thisEl.label + "." + eachRow.label
                        thisAlt = strip_tags(eachRow.cdata)
                        if definition not in [D['definition'] for D in nestedInfo]:
                            thisInfo = {'marker': marker, 'definition': definition, 'alt': thisAlt, 'parent': eachY}
                            nestedInfo.append(thisInfo)
        # Prepare nesting
        allNests = []
        if len(allY) == 1:
            myParents = getParentInfo(nestedInfo, allY[0], 'marker')
            for eachParent in myParents:
                thisNest = {'parentMarker': eachParent, 'parent': allY[0]}
                allNests.append(thisNest)
        else:
            for maxIndex in range(len(allY)-1,0,-1): # Start at inner most nest
                # The parent is the next outer nest
                if maxIndex == 1:
                    myParents = getParentInfo(nestedInfo, allY[0], 'marker')
                    parentLabel = allY[0]
                else:
                    myParents = getParentInfo(nestedInfo,allY[len(allY) - maxIndex], 'marker')
                    parentLabel = allY[len(allY) - maxIndex]
                for eachParent in myParents:
                    thisNest = {'parentMarker': eachParent, 'children': [], 'parent': parentLabel}
                # Create dictionary with 'parent': Parent nest marker, 'children': children markers
                    myChildren = []
                    for eachItem in nestedInfo:
    
                    # If this item's parent is the Y we're looking for
                        if eachItem['parent'] == allY[maxIndex]:
                            myChildren.append(eachItem['marker'])
    
                    thisNest['children'] = myChildren
                    allNests.append(thisNest)

# allNests now has the marker breakdowns for the nest

        # Write to spreadsheet
        nestedSheet.write(curRow,curCol,"#="+tableTitle) # Table Header

        # For each Y axis group

        headerRow = curRow
        curRow+=1
        startingRow = curRow
        tempCol = 1


        for YIndex in range(len(allY)-1,-1,-1):
            for eachNest in allNests:
                if eachNest['parent'] == allY[YIndex]:
                    nestedSheet.write(curRow, YIndex, eachNest['parentMarker'])
                    if len(allY) == 1:
                        curRow += 1
                    else:
                        for eachChild in eachNest['children']:
                            nestedSheet.write(curRow,YIndex + 1 , eachChild)
                            curRow += 1
                            

        # Write headers
        for yIndex in range(1, len(allY)):
            nestedSheet.write(headerRow,yIndex,'#') # Non-limit header
            tempCol += 1
        for xHeader in nestedInfo:
            if xHeader['parent'] == XAxis:
                nestedSheet.write(headerRow, tempCol, xHeader['marker']) # Write X-Axis markers at table headings
                for y in range(startingRow, curRow):
                    nestedSheet.write(y,tempCol, 'inf') # Set limits
                tempCol += 1
        curRow += 1
        
        print "Created nested quotas on sheet: Nested"
                
                    
                

# allQs now has all the information we need in regards to question / Variable quotas
# allSS has all we need in regards to samplesource quotas 
# Now we write the quota sheet


# Overalls Define
definesSheet.write(0, 0, "Total")
definesSheet.write(0, 1, "1")
definesSheet.write(0, 2, "Total Respondents")


#OVERALL QUOTA TRACKER
quotasSheet.write(0,0,"#=Overall")
quotasSheet.write(1,0,"Total")
quotasSheet.write(1,1,'inf')

if args.verbose:
    print "Created Overall table. Marker: Total, Limit: inf (this is default)"

definitions = [] # Used to prevent duplicate definitions
# Sample sources
quotasSheet.write(3,0,"#=Samplesources")
allSSCount = len(allSS) + 1
for i in range(1, allSSCount): # Start at one to account for the "TOTAL"
    # Definitions
    definesSheet.write(i, 0, allSS[i-1]['markerlabel'])
    definesSheet.write(i, 1, "list=='"+allSS[i-1]['lvalue']+"'")
    definesSheet.write(i, 2, allSS[i-1]['title'])
    definitions.append("list=='"+allSS[i-1]['lvalue']+"'")
    # Quotas
    quotasSheet.write(i+3, 0, allSS[i-1]['markerlabel'])                      
    quotasSheet.write(i+3, 1, allSS[i-1]['limit'])
    if args.verbose:
        print "Created marker %s for %s with title %s and set limit to %s." % (allSS[i-1]['markerlabel'],"list=='"+allSS[i-1]['lvalue']+"'",allSS[i-1]['title'],allSS[i-1]['limit'])


# Question Quotas
allQCount = len(allQs) + 1
counter = 0
toWrite = 1
for i in range(1,allQCount):
    # Defines
    if allQs[i-1]['markerdefinition'] != '1' and allQs[i-1]['markerdefinition'] not in definitions:
        definitions.append(allQs[i-1]['markerdefinition'])
        definesSheet.write(toWrite+allSSCount-1, 0,allQs[i-1]['markerlabel'])
        definesSheet.write(toWrite+allSSCount-1, 1,allQs[i-1]['markerdefinition'])
        definesSheet.write(toWrite+allSSCount-1, 2,allQs[i-1]['altLabel'])
        toWrite += 1
    # Quotas
    # Check which markers belong together, if it does only add to current quota table else make new quota table
    # If the curren't dict's title doesn't match the previous they don't belong together
    if (i > 1 and allQs[i-1]['title'] != allQs[i-2]['title']) or (i==1):
        if counter != 0:
            counter+=1   # Account for the space if it's a new table
        quotasSheet.write(i+allSSCount+counter+3, 0, "#="+allQs[i-1]['title'])  # Plus 3 to account for Overall table and the additional space
        quotasSheet.write(i+allSSCount+counter+4, 0, allQs[i-1]['markerlabel'])
        quotasSheet.write(i+allSSCount+counter+4, 1, allQs[i-1]['qlimits'])
        counter+=1
    else: # This limit is for the same question
        quotasSheet.write(i+allSSCount+counter+3, 0, allQs[i-1]['markerlabel'])
        quotasSheet.write(i+allSSCount+counter+3, 1, allQs[i-1]['qlimits'])
    if args.verbose:
        print "Created marker %s for %s with title %s and set limit to %s." % (allQs[i-1]['markerlabel'],allQs[i-1]['markerdefinition'],allQs[i-1]['altLabel'],allQs[i-1]['qlimits'])

print "Created quotas on sheet: Quotas"



# Nested Definitions
if args.nest:
    nestCount = len(nestedInfo)
    toWrite = 0
    for i in range(0, nestCount):
        adjusted = toWrite + allQCount + allSSCount - 1
        if nestedInfo[i]['definition'] != '1' and nestedInfo[i]['definition'] not in definitions:
            definitions.append(nestedInfo[i]['definition'])
            definesSheet.write(adjusted, 0, nestedInfo[i]['marker'])
            definesSheet.write(adjusted, 1, nestedInfo[i]['definition'])
            definesSheet.write(adjusted, 2, nestedInfo[i]['alt'])
            toWrite += 1
        if args.verbose:
            print "Created marker %s for %s with title %s." % (nestedInfo[i]['marker'],nestedInfo[i]['parent'],nestedInfo[i]['definition'])



try:
    quotaWorkbook.save(myDir+"/quota.xls")
except:
    exit("Error saving spreadsheet.")


print "quota.xls created! Thanks for flying Vic!"
