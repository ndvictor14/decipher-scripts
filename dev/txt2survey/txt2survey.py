#!/usr/bin/python

#This script will hopefully take a .txt file and convert it to a full survey :D

import sys
import os
import optparse # handle options
import subprocess
import questionTypes


def create(questionType):
    if questionType == "CHECKBOX":
        checkbox()

parser = optparse.OptionParser()

# Options
parser.add_option('-f','--file',dest='myFile', help='Text file containing the survey to be programmed.')

(options,args) = parser.parse_args()




keywords = ['CHECKBOX'] # Used for testing
#keywords = ['CHECKBOX','FLOAT','LOOP','NUMBER','RADIO','SELECT','TEXT','TEXTAREA']

# Check that a file is specified and is in the directory
if not options.myFile:
    print "Please specify a file name. Use txt2survey -h for more information.\n\n"

if not os.path.isfile(options.myFile):
    path = subprocess.Popen('pwd', stdout=subprocess.PIPE)
    text = path.stdout.read()
    print "File %s " % options.myFile + " does not appear to exists in %s" % text


# For accurate parsing - remove any double spaces
#subprocess.call(["sed -i 's/  / /'", "options.myFile"])


# Go through the file and do the magic
with open(options.myFile) as f:
    for line in f:
        print line
        if line in keywords:
            create(line)
            
