#!/usr/bin/python


# This script will find mismatched xml tags and print out the location of mismatches

import os
import optparse
import re
import operator

parser = optparse.OptionParser()

#Options
parser.add_option('-f','--file',dest='myFile',help='File to analyze.')
(options,args) = parser.parse_args()

print options.myFile

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

# Create dictionaries to hold line numbers and actual tags
openTags = {}
closeTags = {}

# regular expressions
openTag = re.compile('<[a-zA-Z0-9]*>')
closeTag = re.compile('<\/.*>')

lineNumber = 0

with open(options.myFile) as f:
  content = f.readlines()
  for eachItem in content:
    lineNumber += 1
    openMatches = openTag.match(eachItem)
    closeMatches = closeTag.match(eachItem)
    if openMatches:
      openTags[eachItem] = lineNumber
    if closeMatches:
      closeTags[eachItem] = lineNumber

sortedOpenTags = sorted(openTags.items(), key=operator.itemgetter(1))
sortedCloseTags = sorted(closeTags.items(), key=operator.itemgetter(1))

def removekey(d, key):
    r = dict(d)
    del r[key]
    return r


for eachOpenKey in sortedOpenTags:
  for eachClosedKey in sortedCloseTags:
    if eachOpenKey[0].strip('<>') == eachClosedKey[0].strip('</>'):
        sortedOpenTags = removekey(sortedOpenTags,eachOpenKey)
        sortedCloseTags = removekey(sortedCloseTags,eachClosedKey)
