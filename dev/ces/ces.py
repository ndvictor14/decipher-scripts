#!/usr/bin/python2.7

#######################################################################
# CES STUDY CLONE SCRIPT                                              #
# Author: Victor Hernandez (vhernandez@decipherinc.com)               #
#######################################################################

import argparse
import os
import re

# Function for outputting errors
def usage(error):
    print error
    print "For usage assistance: ./ces.py -h"
    exit()


# Function for printing some additional reminders upon successful run
def success():
    print "Don't forget to update your images, survey name, and alt!"
    print "Thanks for flying Vic ;D"

parser = argparse.ArgumentParser(description="CES Clone options.")
parser.add_argument('-m','--magazine', help='CES magazine title')
parser.add_argument('--labels', '--labels', help='Specify question labels if you don\'t want to include all of the questions (comma separated list)')

args = parser.parse_args()


specified = False 
validMags = ['ab', 'ar', 'bh', 'ew', 'fc', 'ff','ft', 'ml', 'mo', 'pa', 'rr', 'th']

class SurveyTemplate(Template):
    delimiter = '~'

# This function breaks out key/value pairs from the defaults file
def read_subsitutions(filename, delimiter="\t"):
    col_vars = []
    rows_data = {}
    with open(filename, 'r') as csvfile:
        rows = csv.reader(csvfile, delimiter=delimiter)
        for i, row in enumerate(rows):
            if i == 0:
                col_vars = row
            else:
                this_row = {}
                if len(row) != len(col_vars):
                    print "Malformed tab-delimited file."
                    print "%s, line %d:  %s" % (filename, i, "    ".join(row))
                    terminate_script()
                for j, item in enumerate(row):
                    this_row[col_vars[j]] = item.strip()
                rows_data[i + 1] = {}
                rows_data[i + 1].update(this_row)
    return col_vars, rows_data



# Handle the magazine label
if not args.magazine:
    exit("Please specify a magazine.")
else:
    args.magazine = args.magazine.lower()

if args.magazine not in validMags:
    usage("Magazine specified is not valid. Possible values are: %s" % validMags)


altRegex = re.compile('.*alt=.*')
altTitle = ""
# Handle question labels
questions = []
if args.labels:
    specified = True
    args.labels = args.labels.lower()
    questions = args.labels.split(',')

if open("survey.xml","r"):
    with open("survey.xml") as surveyFile:
        for line in surveyFile:
            if re.match(altRegex,line):
                altTitle=line
surveyFile.close()

output = open("ces.xml", "w")
print read_substitutions('defaults.txt')

#with open("/home/jaminb/v2/temp/victor/scripts/dev/ces/template.xml") as survey:
#    line = survey.next()
#    line = survey.next()
#    while not re.match(r'.*>',line ):
#        if re.match(r'.*alt=.*',line):
#            output.write(altTitle)
#            line = survey.next()
#        else:
#            output.write(line)
#            line = survey.next()
#
#    output.write(line)
#    line = survey.next()

#    for eachLine in survey:
#        if re.match(r'.*<samplesources>.*',eachLine):
#            output.write(eachLine)
#        if re.match(r'.*%s.*' % options.magazine):
#            output.write
#        if re.match(r'.*</samplesources>.*',eachLine):
#            output.write(eachLine)

#if not args.labels:
#    if args.magazine != "mo":
#        print "Not more"

