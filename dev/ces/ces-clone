#!/usr/bin/python


#############################################################
# Author: Victor Hernandez (modification of clone-email)    #
#############################################################

import csv
import os.path
import pprint
import re
import sys
from optparse import OptionParser
from string import Template


if __name__ != '__main__':
    print "clone-email can not be used as a library"
    exit();

# whatIsBroken = '''
# magazine code / list should be included as a key item in the vars list
# gen_template_dict - use a dict rather than if exception for 'ml' to 'mw'
# do proper template cleaning instead of: 
#     temp_template = [L.replace('’', "'") for L in f.readlines()]
# Place template generator into its own function
# Output error messages to stderr
# Add "_safe" to -v output and document how it works.
# '''

magazine_types = ['ab', 'ar', 'bh', 'ew', 'fc', 'ff',
                  'ft', 'ml', 'mo', 'pa', 'rr', 'th']
unsafe_symbols = "©®™"
destination_pattern = re.compile('survey.xml')
label_pattern = re.compile("\S*LABEL\S*\.txt$")
pp = pprint.PrettyPrinter(indent=4)


class Color:
    RED = '\033[31m'
    YELLOW = '\033[33m'
    GREEN = '\033[32m'
    RESET = '\033[0m'

    @staticmethod
    def color(s, c):
        return ''.join([c, s, Color.RESET]);


class SurveyTemplate(Template):
    delimiter = '~'


def read_table(filename, delimiter="\t"):
    col_vars = []
    rows_data = {}
    with open(filename, 'r') as csvfile:
        rows = csv.reader(csvfile, delimiter=delimiter)
        for i, row in enumerate(rows):
            if i == 0:
                col_vars = row
            else:
                this_row = {}
                if len(row) != len(col_vars):
                    print "Malformed tab-delimited file."
                    print "%s, line %d:  %s" % (filename, i, "    ".join(row))
                    terminate_script()
                for j, item in enumerate(row):
                    this_row[col_vars[j]] = item.strip()
                rows_data[i + 1] = {}
                rows_data[i + 1].update(this_row)
    return col_vars, rows_data

def get_args():
    parser = OptionParser()
    parser.add_option("-g", "--generate", dest="gen",
                      help="Generate Template",
                      metavar="FILE")
    parser.add_option("-v", "--vars", dest="vars",
                      help="write variables to screen. No files will be generated",
                      action="store_true")
    parser.add_option("-p", "--preview", dest="preview",
                      help="preview output. No files will be generated.",
                      action="store_true")
    parser.add_option("-x", "--extras", dest="extras",
                      help="File containing new and/or overriding variables",
                      metavar="FILE")
    parser.add_option("-s", "--src", dest="source",
                      help="create email(s) from this source file. ie clone-template.txt",
                      metavar="FILE")
    parser.add_option("-d", "--dest", dest="destination",
                      help="name(s) of the created destination file(s). LABEL replaced with magazine type. ie email-LABEL.txt",
                      metavar="FILE")
    return parser.parse_args()

def gen_template_dict(code):
    d = {}

    for v in the_variables:
        if v['type'] == code:
            d[v['key']] = v['value']
            d['code'] = code
            d['list'] = code

    d = dict(d.items() + create_safe_keys_values(d).items())
    update_symbols(d)
    return d

def update_symbols(d):
    symbols = {
        '©': '&copy;',
        '®': '<sup>&reg;</sup>',
        '™': '&trade;',
    }

    for k, v in d.iteritems():
        for sk, sv in symbols.iteritems():
            if sk in v:
                d[k] =  v.replace(sk, sv);  

def create_safe_keys_values(d):
    safe = {}
    for k, v in d.iteritems():
        temp = v
        for symbol in unsafe_symbols:
            temp = temp.replace(symbol, '')  
         
        safe["%s_safe" % k] = temp
    return safe

def gen_template_dict_bak(code):
    d = {}

    for v in the_variables:
        if v['type'] == code:
            d[v['key']] = v['value']
        d['code'] = code

    return d

def show_preview(template, code):
    output = ''
    d = gen_template_dict(code)

    for k, v in d.iteritems():
        d[k] = Color.color(v, Color.YELLOW)

    try:
        output = SurveyTemplate(template).substitute(d)
        print output
    except KeyError, e:
        print "\nError. Key not found in template: %s " % str(e)
        terminate_script()
    except ValueError, e:
        print "\nError. Value not found in template: %s " % str(e)
        terminate_script()
    except:
        print "\nSomething went wrong with preview, don't know what."
        terminate_script()

def updateExtras():
    n_columns = 3
    if options.extras:
        if not os.path.isfile(options.extras):
            print "Extras not found."
            exit()

        keys, values = read_table(options.extras)
              
        for i, v in enumerate(values.itervalues()):
            if len(v) != n_columns:
                print options.extras + " is not well formated: line " + str(i)
                print Color.color("\nNo files were generated.", Color.RED)
                terminate_script()

        for value in values.itervalues():
            t, k, v = (value[x] for x in ['type', 'key', 'value'])
            index = get_index(t, k)

            if index != None:
                the_variables[index]['value'] = v
            else:
                the_variables.append({'type': t, 'key': k, 'value': v})

def validate_file(filename, display_error=True):
    if not os.path.isfile(filename):
        if display_error:
          print filename + " not found."
        return False
    return True

def verify_overwrite_existing(filenames):
    # Check which files will be overwritten
    already_exists = []
    for f in filenames:
        if validate_file(f, display_error=False):
            already_exists.append(f)

    # If file(s) exist, display them and prompt user to overwrite
    if already_exists:
        print "The following files will be overwritten:"
        for f in already_exists:
            print ''.join(["\t", f])
        print "\nDo you want to proceed? [y/N] ",
        choice = raw_input().lower()

        if not choice or choice[0] not in ['y', 'yes']:
            print Color.color("\nNo files were generated.", Color.RED)
            exit()

def generate_files(emails):
    # Create files
    for filename, output in emails.iteritems():
        with open(filename, 'w') as f:
            f.write(output)
            print "created " + filename

def generate_emails(template, the_args):
    destination_pre = ''
    destination_post = ''

    tokens = destination_pattern.match(options.destination)
    if tokens:
        destination_pre = tokens.group(1)
        destination_post = tokens.group(2)

    emails = {}
    for a in set(the_args):
        d = gen_template_dict(a)
        filename = ''.join([destination_pre, a, destination_post])

        try:
            emails[filename] =  SurveyTemplate(template).substitute(d)
        except KeyError, e:
            print "\nError. Key not found in template: " + str(e)
            terminate_script()
        except:
            print "\nSomething went wrong, don't know what."
            terminate_script()
    return emails

def get_value(type, key):
    for d in the_variables:
        if d['type'] == type and d['key'] == key:
            return d['value']

def get_index(type, key):
    for i, d in enumerate(the_variables):
        if d['type'] == type and d['key'] == key:
            return i
    return None

def import_defaults_data():
    script_path = os.path.dirname(os.path.abspath(__file__))
    defaults_path = '%s/clone-email-defaults.txt' % script_path

    if os.path.isfile(defaults_path):
        cols, rows = read_table(defaults_path)
        data = []
        for i in sorted(rows):
            data.append(rows[i])
        return data
    else:
        print 'clone-email-defaults.txt did not import'
        terminate_script()

def merge_all_variables():
    types = []
    for v in the_variables:
        t = v['type']
        if t != 'all' and t not in types:
            types.append(t)

    for v in (v for v in the_variables if v['type'] == 'all'):
        for t in types:
            if get_index(t, v['key']) == None:
                the_variables.append({'type': t, 'key': v['key'], 'value': v['value']})

def display_variables():
    headers = ['type', 'key', 'value']
    widths = []
    filtered_vars = filter(lambda v: v['type'] in the_args, the_variables)

    for h in headers:
        widths.append(max([len(s) for s in [d[h] for d in filtered_vars]]))

    for a in the_args:
        rows = filter(lambda v: v['type'] == a in the_args, filtered_vars)

        for r in sorted(rows, key=lambda k: k['key']):
            for t, w in zip(headers, widths):
                print ("%s" % r[t]).ljust(w + 1),
            print
    
    print Color.color('Variable view only. File(s) not created.', Color.YELLOW)
    exit()

def validate_args():
    # Validate 1 or more args
    global the_args
    if not the_args:
        print "You must specify one or more magazine codes."
        return False
    
    # Check for 'all' arg
    if len(the_args) == 1 and the_args[0] == 'all':
        the_args = list(magazine_types)
    elif 'all' in the_args:
        print "You may only select 'all' exclusively."
        return False
    else:
        # Check that arg(s) is a valid magazine type
        for a in the_args:
            if a not in magazine_types:
                print "'%s' is not a valid magazine type." % a
                return False
    # Sort args
    the_args = list(set(the_args))
    the_args.sort()
    return True

def terminate_script():
    print Color.color("\nProcess terminated.", Color.RED)
    exit()

if __name__ == '__main__':
    print "\nwelcome to clone-email\n"
    the_variables = import_defaults_data()
    options, the_args = get_args()

    # Generate template and exit
    if options.gen:
        if validate_file(options.gen, False):
            print Color.color("\nFile already exists. Remove file before continuing.", Color.RED)
        else:
            script_path = os.path.dirname(os.path.abspath(__file__))
            master_path = '%s/clone-template-master.txt' % script_path
            master_template = ""

            if os.path.isfile(master_path):
                with open(master_path, 'r') as f:
                    master_template = ''.join(f.readlines())
                with open(options.gen, 'w') as f:
                    f.write(''.join(master_template))
                    print Color.color("\ncreated %s" % options.gen, Color.GREEN)
        exit()

    # Validate args
    did_validate = validate_args()

    # Validate Source Files
    for f in [options.source, options.extras]:
        if f:
            did_validate = validate_file(f) and did_validate
    
    if options.destination and options.destination == options.source:
        print "Destination file name can not be the same as the source file."
        did_validate = False

    if options.destination and options.destination == options.extras:
        print "Destination file name can not be the same as the extras file."
        did_validate = False

    # x_LABEL.txt
    if options.destination:
        m = label_pattern.match(options.destination)
        if not m:
            print "Destination not valid filename. Must contain 'LABEL' in filename and end with .txt'. (i.e. email_LABEL.txt)"
            did_validate = False
   
    if not (options.source or options.destination or options.extras or options.vars or options.preview):
        print "You must specify option(s)."
        did_validate = False 

    # Exit if validation failed 
    if not did_validate:
        print Color.color("\nPlease fix issues before trying again. Use clone_emails -h for help.", Color.RED)
        terminate_script()

    # Extras and the merging of the all variables
    updateExtras()
    merge_all_variables()

    # Show variables
    if options.vars:
        display_variables()

    # Load template
    temp_template = []
    if options.source:
        with open(options.source, 'r') as f:
            temp_template = [L.replace('’', "'") for L in f.readlines()]
    template = ''.join(temp_template)

    # Show preview
    if options.preview and options.source:
        for a in the_args:
            show_preview(template, a)
            print
        print Color.color('Preview only. File(s) not created.', Color.YELLOW)
        exit()

    # Create files
    if options.source and options.destination:
        emails = generate_emails(template, the_args)
        verify_overwrite_existing(emails.keys())
        generate_files(emails)
        print Color.color("Success!!", Color.GREEN)
