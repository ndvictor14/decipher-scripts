# RANDSEND script

## Purpose
-----------
The purpose of this script is to use a sample file and select X number of *SENDABLE* emails from the file. In addition, the script has an option to split the new sample file based on a variable, much like coladd but in a more targeted manner. For example if you have a file with 30,000 emails and you want to select 15,000 random files and add a variable "tracker" split evenly among 3 subsets of the 15,000, the script will be able to accomplish this for you. 