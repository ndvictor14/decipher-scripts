#!/usr/bin/python2.7

########################################################
# Author: Victor Hernandez (vhernandez@decipherinc.com)#
# Date: March 4, 2015                                  #
########################################################

import os
import argparse
import subprocess
import re
import fileinput


parser = argparse.ArgumentParser(description="randsend options")
parser.add_argument('-f','--file', help="sample file")
parser.add_argument('-n','--num', type=int, help="desired sample size")
parser.add_argument('-s','--splits')
parser.add_argument('-v','--verbose', help="verbose output", action="store_true")
parser.add_argument('-m','--man', help="Open the scripts man page", action="store_true")
parser.add_argument('-o','--outputFile', help="Name of output file - defaults to randsendSample.txt", default="randsendSample.txt")
options = parser.parse_args()


if options.man:
    subprocess.call(['man', '/home/jaminb/v2/temp/victor/scripts/manpages/randsend'])

if not options.file:
    exit("Please specify a sample file to use.")
if not options.num:
    exit("Please specify the desired sample size.")


splitSyntax = re.compile('.*:.*,?[$^\d\w]')

mysplits = []
newheader=""
limits = options.num

if options.splits:
    if not re.match(splitSyntax,options.splits):
        exit("Invalid split syntax. \n\nFormat: newHeader:split1,split2,split4 or newHeader:split1=count1,split2=count2....\n\n")
    else:
        newheader = options.splits.split(':')[0]
        mysplits = options.splits.split(':')[1].split(',')
        limits = options.num / len(mysplits)
headers = []

#ensure clean format of file
if options.verbose:
    print "Converting to unix format to ensure formatting...\n"
subprocess.call(['dos2unix',options.file])
# Run bulk filter to minimize number of bad emails
if options.verbose:
    print "Running bulk filter to limit bad emails"
#subprocess.call(['bulk', 'filter', options.file,'good.txt','bad.txt'])

temp1 = open('temp1.txt','w')
outputFile = open(options.outputFile,'w')
specifiedLimits = False

for eachSplit in mysplits:
    if "=" in eachSplit:
        specifiedLimits = True

with open('good.txt') as sampleFile:
    line = next(sampleFile) # ignore header
    headers = line.split('\t')
    for line in sampleFile:
        temp1.write(line)

temp1.close()



# shuffle the file and grab however many rows are needed 


# This will hold an array of all the temp files created so that they can easily be removed afterwards
tempFiles = []


def capture_stdout(stdin, outputFile):
    for line in stdin:
        outputFile.write(line)

def line_prepender(filename, toAddList):
    myText = ""
    for eachHeader in toAddList:
        if myText == "":
            myText = eachHeader
        else:
            myText = myText + "\t" + eachHeader
    with open(filename,"r+") as f:
        content = f.read()
        f.seek(0, 0)
        f.write(myText + content)

def remove_headers(stdin):
    tempFile = "randsendTemp.txt"
    temp = open(tempFile,"w")
    with open(stdin,"r+") as myFile:
        line = myFile.next()
        for line in myFile:
            temp.write(line)
    temp.close()
    subprocess.call(['mv',tempFile,stdin])
        

if options.splits:
    # Run command per value given
    for eachValue in mysplits:
        # if values were specified then the command has to be executed with the given values
        if specifiedLimits:
            vals = eachValue.split('=')[1]
            tempFile = str(eachValue.split('=')[0]) + ".txt"
            tempFiles.append(tempFile)
            tempOut = open(tempFile,"w")
            sortProcess = subprocess.Popen(('sort', '-R', 'temp1.txt'), stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            headProcess = subprocess.Popen(('head', '-n', vals), stdin=sortProcess.stdout, stdout=subprocess.PIPE)
            capture_stdout(headProcess.stdout,tempOut)
            headProcess.wait()
            headProcess.stdout.close()
            tempOut.close()
            # Add headers back in 
            line_prepender(tempFile,headers)
            # Up to here works
            colCmd = "coladd -v "+tempFile+":"+newheader+"="+eachValue.split('=')[0]
            if options.verbose:
                print "Executing : %s" % colCmd
            addCol = subprocess.Popen([colCmd],stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
            temp = "temp.txt"
            temp = open(temp,"w")
            capture_stdout(addCol.stdout,temp)
            addCol.wait()
            addCol.stdout.close()
            temp.close()
            subprocess.call(['mv','temp.txt',tempFile])
            remove_headers(tempFile)

        # If limits weren't specified we can split the counts evenly
        else:
            tempFile = str(eachValue) + ".txt"
            tempFiles.append(tempFile)
            tempOut = open(tempFile,"w")
            sortProcess = subprocess.Popen(('sort', '-R', 'temp1.txt'), stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            headProcess = subprocess.Popen(('head', '-n', str(limits)), stdin=sortProcess.stdout, stdout=subprocess.PIPE)
            capture_stdout(headProcess.stdout,tempOut)
            headProcess.wait()
            headProcess.stdout.close()
            tempOut.close()
            # Add headers back in 
            line_prepender(tempFile,headers)
            temp = "temp.txt"
            colCmd = "coladd -v "+tempFile+":"+newheader+"="+eachValue+" > "+temp
            #tempted = open(temp,"w")
            if options.verbose:
                print "Executing: %s" % colCmd
            subprocess.call([colCmd],shell=True,stderr=subprocess.PIPE)
            #addCol = subprocess.Popen([colCmd],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            #capture_stdout(addCol.stdout,tempted)
            #addCol.wait()
            #addCol.stdout.close()
            #tempted.close()
            #tempFile.close()
            subprocess.call(['mv',temp,tempFile])
            if options.verbose:
                print "Executing: mv %s %s" % (temp, tempFile)
            remove_headers(tempFile)

else:
    # Nothing special just run the sort and head commands
    sortProcess = subprocess.Popen(('sort', '-R', 'temp1.txt'), stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    headProcess = subprocess.Popen(('head', '-n', str(options.num)), stdin=sortProcess.stdout,stdout=subprocess.PIPE)
    capture_stdout(headProcess.stdout, outputFile)
    headProcess.wait()


# Cat the temp files into the final file
counter = 1
if len(tempFiles) == 1:
    subprocess.call(['mv',tempFiles[0],options.outputFile])

# If there are more than one temp files cat them into the final file
elif len(tempFiles) > 1:
    toCat = "cat "
    for eachFile in tempFiles:
        toCat += eachFile + " " 
    toCat += " > " + options.outputFile
#    outFile = open(options.outputFile)
    catProcess = subprocess.call([toCat],shell=True)
#    capture_stdout(catProcess.stdout,outFile)
#    catProcess.wait()
#    catProcess.stdout.close()
#    outFile.close()

# add headers
headers.append(newheader)
for eachIndex in range(0,len(headers)):
    if eachIndex != len(headers) - 1:
        headers[eachIndex] = headers[eachIndex].strip('\n')
    else:
        headers[eachIndex] = headers[eachIndex] + "\n"
line_prepender(options.outputFile,headers)

if options.verbose:
    print "File %s generated." % options.outputFile

# Clean up
if len(tempFiles) != 0:
    # Remove temp cats
#    catsInTheBag = subprocess.Popen(['ls','cat*.txt'],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
#    howManyCats = subprocess.check_output(['wc','-l'],stdin=catsInTheBag.stdout,stderr=subprocess.PIPE,stdout=subprocess.PIPE)
#    print howManyCats.stdout
#    removeCats = "rm cat*.txt"
    removeCats = subprocess.Popen(['rm','cat*.txt'] , stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    removeCats.wait()
    removeCats.stdout.close()
    # Remove temp files
    for eachFile in tempFiles:
        removeTemps = subprocess.Popen(['rm',eachFile], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        removeTemps.wait()
        removeTemps.stdout.close()

if options.verbose:
    print "Temporary files removed."                  
exit("Process Finish.\nThanks for flying Vic :D")
       
