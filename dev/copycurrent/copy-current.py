#!/usr/bin/env hpython

author = """

$$\    $$\ $$\             $$\                         
$$ |   $$ |\__|            $$ |                        
$$ |   $$ |$$\  $$$$$$$\ $$$$$$\    $$$$$$\   $$$$$$\  
\$$\  $$  |$$ |$$  _____|\_$$  _|  $$  __$$\ $$  __$$\ 
 \$$\$$  / $$ |$$ /        $$ |    $$ /  $$ |$$ |  \__|
  \$$$  /  $$ |$$ |        $$ |$$\ $$ |  $$ |$$ |      
   \$  /   $$ |\$$$$$$$\   \$$$$  |\$$$$$$  |$$ |      
    \_/    \__| \_______|   \____/  \______/ \__|   

""" 



import pkg_resources, hstub
from hermes import Survey, misc
from util import fatal
import argparse
import os
import sys
import subprocess

if len(sys.argv) < 2:
    exit("Specify new directory name.")

del sys.argv[0]

surveyName = "."

surveyName = misc.expandSurveyPath(surveyName)
print surveyName
theSurvey = Survey.loadNoException(surveyName)
print theSurvey
if theSurvey == None:
    exit("Unable to open specified survey: %s" % surveyName)


newDirPath = theSurvey.path

for argument in sys.argv:
    if '-' not in argument[0]:
        newDirPath += argument
        subprocess.call(['mkdir', newDirPath], shell=True)
        subprocess.call(['cd',newDirPath], shell=True)


newFile  = open('new.xml','w')

for eachQ in theSurvey.root.qElements:
    if eachQ.where.survey or eachQ.where.execute:
        newFile.write(eachQ)
        
newFile.close()
