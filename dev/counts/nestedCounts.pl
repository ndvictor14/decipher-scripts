#!/usr/bin/perl
# Author: Victor Hernandez (vhernandez@decipherinc.com)
# Date: May 25, 2014
# Description: This file gets counts of unique items based on column headers

#use strict;
use Data::Dumper qw(Dumper);
use Getopt::Std;

#sub routine for printing usage options
sub usage {
    my $message = $_[0];
    if (defined $message && length $message) {
      $message .= "\n"
	  unless $message =~ /\n$/;
    }

    my $command = $0;
    $command =~ s#^.*/##;

   print STDERR (
      $message,
      "usage: $command -f file1,file2,etc. -headers header1,header2,etc. [-b] [-u [header1,header2,etc.]] [-r header1,header2,etc.] [-o outFileName]" .
      "       ...\n" .
      "       ...\n" .
      "       ...\n"
       );

   die("\n")
}


my %opts = ();

getopt('vbfh:h:u:r:o:', \%opts); # -f takes filenames (NOTE: Files must have matching header NAMES (same header numbers not necessary) if doing nested counts), -headers takes headers to count, b flags wheter to count blanks, -u takes arguments and tells whether to print a count of uniques in this column, -r takes arguments to remove any rows containing empty fields in the specified columns, the v flag allows for a verbose output, -o allows the specification of the output filename. 

# Get counts of specified unique columns

#Usage errors
usage("Please specify file(s) using -f file1,file2,etc.\n")
    unless defined $opts{f};
usage("Please specify column headers to count using -headers header1,header2,etc.\n")
    unless defined $opts{h};
#Check that the right number of arguments were given ie filename and at least one header

my $v = $opts{v}; #Verbose flag
my @files = split(',',$opts{f}); # File list
my @colSearch = split(',', $opts{h}); # Column headers
my @uCountHeaders = split(',', $opts{u}); # Unique Column Headers Counts
my @removeEmptyRowHeaders = split(',', $opts{r}); # Columns to search for empty rows in 

if (defined $opts{o}){
    my $outputFileName = $opts{o};
    # Duplicate filename handling
    if (-e "$outputFileName"){
	print "WARNING: File $outputFileName already exists!\n\nWould you like to overwrite it? (Y|N) ";
	my $vefy = <>;
	if ($vefy =~ m/n|N|no|NO|nO|No/){
	    print "Creating output.txt.";
	    open(OUT, ">", "output.txt");
	}
	else{
	    open(OUT, ">", $outputFileName);
	}
    }
    else{
	open(OUT, ">", $outputFileName);
    }
}

#Default output file name 
else{
    open (OUT, ">", "countOut.txt");
}

my $searchCount = scalar(@colSearch);
my $tmp = 0;

#Ensure file(s) exists and is/are readable
for my $file (@files){
    if (-e $file){
	if ($opts{v}){
	    print "File: $file Found!\n";
	}
	open(FILE, "<", $file)
	    or die("Unable to open file.\n");
    }
    else{
	die("The file $file does not seem to exists....\n");
    }
}

#Let's make sure headers are found along all files
for my $file (@files){
    if ($v){
	print "Verifying headers across files...\n"
    }
    open (FILE, "<" , $file);
    my $firstLine = <FILE>;
    my @headers = split(/\t/, $firstLine);
    foreach my $head(@headers){
	$headers[$tmp] = lc $head;  
	$tmp++;
    }
    my %params = map { $_ => 1} @headers;
    my @colTrackers;
    my $ind = 0;
    #File headers to lower case
    $firstLine = lc $firstLine;

    #Search for header existence in files
    for (my $i = 1; $i <= $searchCount; $i++){
	my $searchString = lc $colSearch[$i];
	if (exists($headers[$searchString])){
	    if ($v){
		print "lc $headers[$i]...header found in $file\n";
	    }
	    ++$ind until $headers[$ind] =~ m/$headers[$i]/ or $ind > $#headers;
	}
	#Header did not exist in file
	else {
	    print "Header not found in $file: lc $headers[$i]\n";
	    die("Possible headers are: $firstLine\n");
	}
}
my $firstLines = <FILE>;
die("DEBugging\n");
	if ($opts{v}){
	    print "Identifying file headers...\n";
	}
	my @headers2 = split(/\t/, $firstLine);
#Ensure that the specified headers are actually in the file -_-
# let's make it more user friendly by ignoring case


for (my $i = 1; $i <= $searchCount; $i++){
    if (exists($params{lc $ARGV[$i]})){
	print "lc $ARGV[$i]...Header found\n";
	++$ind until $headers2[$ind] =~ m/$ARGV[$i]/ or $ind > $#headers2;
    }
    else {
	print "Header not found in file: lc $ARGV[$i]\n";
	die("Possible headers are: $firstLine\n");
    }
    $colTrackers[$i-1] = $ind;
    $ind = 0;
}

#Debugging to see which column number should be printed out 
#print "Tracker: @colTrackers\n";

my %individualCounts = (); #Keep track of individual unique columns
my %nestedCounts = ();  #Keep track of nested counts
my @lineInfo;
my @uniqueCountOnly; #Just track unique, non empty fields
my @blanks; # Keep track of how many blanks for column

# Here we read the file line by line and keep counts of uniques for the specified headers
foreach my $line (<FILE>){
    @lineInfo = split(/\t/, $line);
    foreach my $item (@colTrackers){
#Keep track of individual unique column counts
	#Keep tracks of blanks per column
	if ($lineInfo[$item] eq ""){
	    $blanks[$colTrackers[$item]]++;
	}

	print "Testing: $colTrackers[$item]\n";
    	$individualCounts{$lineInfo[ $item ]} += 1;
	#Keep track of Uniques
	if ($individualCounts{$lineInfo[ $item ] } == 1){
	    $uniqueCountOnly[$item]++;
	}
#This will keep track of NESTED counts based on the first column provided
    }
    my @copy = @colTrackers;
    while (my $token = shift @copy){
	$nestedCounts{$lineInfo[$token]}{$lineInfo[shift @copy]} += 1;
    }
}

print OUT "\nBLANK COUNTS:";

if (scalar @blanks == 0){
    print OUT "\t0\n\n";
}
else{
    foreach my $key (@blanks){
	print OUT "\n\t$key:\t$blanks[$key]\n";
    }
}

print OUT "\nUNIQUE COUNTS:";
foreach my $key (sort keys %individualCounts){
    print OUT "\n\t$key:\t$uniqueCountOnly[$key]";
}

print OUT "Individual Counts:\n\n";
foreach my $key (sort keys %individualCounts){
    print OUT "\t$key: $individualCounts{$key}\n";
}

#print OUT "\nNested Counts:\n\n";
#print OUT Dumper \%nestedCounts;
#foreach my $key (sort keys %nestedCounts){
#    foreach my $otherKey (sort keys %individualCounts){
#	print OUT "\t$key: $nestedCounts{$key}\n";
#    }
}

close(FILE);
close(OUT);

