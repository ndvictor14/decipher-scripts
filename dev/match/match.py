#!/usr/bin/python

##############################################################
# Created By : Victor Hernandez (vhernandez@decipherinc.com) #
##############################################################

import os.path
import sys
from optparts import OptionParser

def get_args():
    parser = OptionParser()
    parser.add_option("-e", "--email", dest="email",
                      help="Send file to this email")
    parser.add_option("-c","--columns", dest="columns",
                      help="column number(s) (starting at 1) or header(s) text to search")
    parser.add_option("-r","--regex",dest="regularExpression",
                      help="Value to match in specified column")
