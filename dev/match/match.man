The match script allows you to print out lines in a file who's specified column match the specified file.

Example:

	You have a file with the headers "email, type, and location" where the type could be any of : "low, normal, high" and location could be any of the US state abbreviations.
	Say you want to look at only respondents from US with a high type. This script could be used for such a thing.
