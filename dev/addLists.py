#!/usr/bin/python

outFile = open('all.txt', 'w')
inFiles = ['val1.csv','val2.csv','val3.csv','val4.csv','val5.csv']
lists = {'godaddy': 1, 'dreamhost': 2, 'wix': 3, 'cloudflare': 4, 'wordpress': 5, 'rackspace': 6, 'digitalocean': 7, 'linode': 8, '1and1': 9}
badTotal = 0
listIdentifier = 'competitor'

for eachFile in inFiles:
    bad = 0
    with open(eachFile,'r') as f:
        headers = f.readline()
        headers = map(lambda x:x.lower().strip('\n'),headers.split('\t'))
        idIndex = headers.index(listIdentifier.lower())
        if inFiles.index(eachFile) == 0:
            for x in headers:
                outFile.write(x + '\t')
            outFile.write('list\n')

        for line in f:
            columns = line.split('\t')
            columns = map(lambda x:x.lower().strip('\n'), columns)
            for eachCol in columns:
                if columns.index(eachCol) == idIndex:
                    try:
                        myList = str(lists[eachCol])
                    except:
                        bad += 1
                        mylist = "bad"
                    outFile.write(eachCol)
                    if columns.index(eachCol) == len(columns)-1:
                        outFile.write('\t'+myList+'\n')
                    else:
                        outFile.write('\t'+myList+'\t')
                        
                else:
                    if columns.index(eachCol) == len(eachCol):
                        outFile.write(eachCol+"\n")
                    else:                            
                        outFile.write(eachCol+"\t")
    badTotal += bad     
    print 'Bad in file %s = %d\n'  % (eachFile,bad)
            
print 'Total Bad: %d' % badTotal
