#/usr/bin/python

__author__ = "Victor Hernandez"

#################################################################
# Module with my helper functions.                              #
#################################################################

def inlineListPrint(theList):
    # Prints a list inline
    theInline = ""
    for x in theList:
        theInline += x
    return theInline


