.\" Manpage for makeQuota.
.\" Contact vhernandez@decipherinc.com to correct errors or typos.
.TH man 8 "15 April 2015" "3.0" "makeQuota man page"
.SH NAME
makeQuota \- create a quota.xls file based of sample sources. or questions
.SH SYNOPSIS
makeQuota [-h] [-v] [-l limit,per,samplesource] [-q QUESTION:limitR1,limitR2,etc QUESTION etc] [-n Y1,Y2/X_Y1/X] [-s] [-e samplesourceVariable:limits:alt]
.SH DESCRIPTION
makeQuota is simple script for creating a basic quota sheet based on sample sources or questions. By default (as should be done in practice) the quota sheet name is "Quotas" so in the survey the quota tag should be <quota sheet="Quotas" overquota="noqual"/>. The script uses inf for default limits. It also supports nested quotas.
.SH OPTIONS
The -h option prints usage syntax. The -v option is a verbose option that prints what it's doing. The -l option takes quota limits for each list. If only one value is given and there are multiple lists, the additional lists will have limits set to inf. If you want to specify a value for each list you can pass the values as comma delimited. For example, if you have 3 lists and you want them to have limits of 25,50, and 100 your command would like like this: makeQuota -l 25,50,100. If for some reason you give more or less values than specified and error will be thrown. If no value is specified, the limits are set to "inf". The script also checks to make sure there is no quota.xls file so that your work isn't accidentally overwritten :) . When using the -q option you can either pass just the question label which will set limits for all rows to inf (this is used when simply tracking groups/categories. Additionally, you can specify limits based on rows. For example if Q1 is your gender question where Q1.r1 is "Male" and Q1.r2 is "Female" and you want 100 Male respondents and 50 Female respondents the syntax would be -q Q1:100,50. This will add a table for Q1 with the set limits. 

The -n option allows you to create nested quotas. If you want to nest your samplesources use 'SS' as your question label. The script supports up to 2 vertically nested quotas and one horizontally nested quotas. Each vertical nest is separated by a ','. The horizontal nest is seperated by an '/'. If you want multiple nested quotas you split them up with a '_'. For example: Q1/SS_Q2/SS would create 2 nested tables - the first Q1 by Samplesources and the second Q2 by Samplesources. The script prompts you for nested table titles. If you would like to skip this you can pass the '-s' option and a default title would be added for you. If nested quotas are created, the sheet is named 'Nested'. 
.SH SEE ALSO

.SH VERSIONS
  3.0 - Current version. Added nested features as well as extra variable options.
  2.0 - Added option to support question quotas.
  1.0 - Create quotas from sample sources

.SH NOTES
  Syntax Example: makeQuota.py -q Q1 -e comp:inf,100,100:Apple,Google,Microsoft -n SS/Q1 -s
  
.SS ""
  This will create:
.TP columns
    Quota sheet with:
          - A Q1 table with all rows in Q1 set to infinite.
          - A comp table with the first value having alt label "Apple" and a limit of inf, the second value have alt label "Google" and a limit of 100, and the value row having an alt label "Microsoft" with a limit of 100. 'comp' being a samplesource variable with 3 values (ie <var name="comp" values="ap,go,ms"/>)
.TP columns   
    Nested sheet with:
	  - A table named "SS/Q1" breaking down Samplesource by Q1 and setting limits to 'inf'.

.TP columns
Normally, you would be prompted for table titles but since the -s option was specified, the prompt is skipped and titles default to the specified arguments ('Q1', 'Q2', 'SS/Q1').

.SH BUGS
Script efficiency can be improved. 

.SH AUTHOR
Victor Hernandez (vhernandez@decipherinc.com)
