#!/usr/bin/env hpython

__author__ = """
      (`-')  _               (`-')                   (`-')  
     _(OO ) (_)     _        ( OO).->       .->   <-.(OO )  
,--.(_/,-.\ ,-(`-') \-,-----./    '._  (`-')----. ,------,) 
\   \ / (_/ | ( OO)  |  .--./|'--...__)( OO).-.  '|   /`. ' 
 \   /   /  |  |  ) /_) (`-')`--.  .--'( _) | |  ||  |_.' | 
_ \     /_)(|  |_/  ||  |OO )   |  |    \|  |)|  ||  .   .' 
\-'\   /    |  |'->(_'  '--'\   |  |     '  '-'  '|  |\  \  
    `-'     `--'      `-----'   `--'      `-----' `--' '--' 
"""

import pkg_resources, hstub
from hermes import Survey, misc
from util import fatal
import argparse
import os
import sys
import subprocess
import xlwt
import re
from xml.dom import minidom
import xml.etree.ElementTree as ET
from HTMLParser import HTMLParser

# Create the survey Object --- Since making a quota should be called within the appropriate directory to avoid incorrectly overwriting quotas for a different project, specified paths will not be allowed 
surveyName = "."

surveyName = misc.expandSurveyPath(surveyName)
theSurvey = Survey.loadNoException(surveyName)
if theSurvey == None:
    exit("Unable to open specified survey: %s" % surveyName)


# function to make friendly markers 
def safeString(theString):
    newString = re.sub(' |-','_',theString)
    return newString


# Function for prompting for quota title
def promptTitle(theCurrentTitle):
    print "Current table title: %s. Input new title or press enter to leave as is." % theCurrentTitle
    if args.skip:
        return theCurrentTitle
    newTitle = raw_input("Table title: ")
    if newTitle == "":
        newTitle = theCurrentTitle
    return newTitle


# ASK ERWIN OR SOMEONE WHETHER THERE IS A METHOD FOR THIS IN SURVEY.PY --- GET 'VALUES' ATTRIBUTE OF SAMPLESOURCE VAR
def findExtras(theExtra):
    doc = getXML()
    xmldoc = ET.fromstring(doc)
    SS = xmldoc.findall('samplesources')
    for SSEl in SS:
        for SEl in SSEl.findall('samplesource'):
            for El in SEl.findall('var'):
                if El.get('name') == theExtra:
                    return El.get('values').split(',')

    return []


# Stuff for cleaning up alts
class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()


def checkOutEl(mySurvey,culprit):
     El = next((x for x in mySurvey.root.qElements if x.label == culprit), None)
     print El.info
     print El.rows
     print El.choices
     return El

# Check if argument is extra variable (as opposed to Question element) and return it if it exists
def checkExtra(theExtra):
    if len(findExtras(theExtra)) == 0:
        exit("Unable to find %s!" % theExtra)
    else:
        return findExtras(theExtra)

def getParentInfo(theArray, targeted, theKey):
    theParents = []
    for eachItem in theArray:
        if eachItem['parent'] == targeted:
            theParents.append(eachItem[theKey])
    return theParents
    


def getXML():
    f = open(surveyName+'/survey.xml', 'r')
    doc = f.read()
    f.close()
    doc = re.sub(r':([^ \s<])',r'\1',doc)
    return doc




# Specify script options
parser = argparse.ArgumentParser(description='Make a quota sheet from the command line.', add_help=False)
parser.add_argument('-l','--limits',help='Comma delimited list of sample quota limits.')
parser.add_argument('-v','--verbose',action="store_true", help="Verbose output.")
parser.add_argument('-h','--help',action="store_true", help="Open manpage.")
parser.add_argument('-q','--questions',nargs="+", help="Questions to add quota limits to. Space delimited. To specify limits/row: Q1:inf,inf,20 will set Q1r1 & Q1r2 limits to inf and Q1r3 limits to 20. If you want to set all row limits to inf only specify question label.")
parser.add_argument('-e','--extra',nargs="+", help="Extra variables from sample sources to track in the quotas. Space delimited. To specify limits/variable values ThisIsMyVar:inf,inf,20 will set the variables first value to inf, the second value to inf and the third value to 20. If you want to set all row limits to inf only specify the variable label. You can specify alt labels for the extra variale's values by using another ':' --- var:limits:\"This is my first alt,Now this is my second alt\".")
parser.add_argument('-n','--nest',  help="Nest quotas. verticalQuestionLabel,verticalQuestionLabel/horizontalQuestionLabel_SecondNestedQuota,nextVertical/BlahHorizontal_etc. Will always default to inf limits. NOTE: USE 'SS' FOR SAMPLESOURCES! ONLY SUPPORTS 2 VERTICAL NESTS!")
parser.add_argument('-s','--skip',action="store_true", help="Don't prompt for table titles.")
args = parser.parse_args()

if args.help:
    subprocess.call(['man','/home/jaminb/v2/temp/victor/scripts/dev/makeQuota2/./makeQuota2'])
    exit()


pkg_resources,hstub




# Check Survey States
if theSurvey.root.state.closed:
    exit("Survey state is closed. If you really want to make changes to this, set it to testing.")
elif theSurvey.root.state.live:
    exit("Survey is LIVE! I will not let you corrupt data. If you're in a temp set it to testing!")

# Check quota existance
if os.path.isfile('/home/jaminb/v2/'+theSurvey.path+'/quota.xls'):
    print "quota.xls found in current directory!\n"
    if not re.match(r'y|yes|Yes|YES|Y',raw_input("Are you sure you want to continue and overwrite? [Y/N] ")):
        exit("Aborted. Thanks for flying Vic.")

myDir = "/home/jaminb/v2/"+theSurvey.path

quotaWorkbook = xlwt.Workbook()
definesSheet = quotaWorkbook.add_sheet('Defines')
quotasSheet = quotaWorkbook.add_sheet('Quotas')



# Grab samplesource information
myss = theSurvey.root.samplesources.children
allSS = []
ssInfo = {}
for eachSS in myss:
    ssMarkerLabel = safeString(eachSS.title)
    ssInfo = {'title': eachSS.title, 'lvalue': eachSS.list, 'limit': 'inf', 'markerlabel': ssMarkerLabel}
    allSS.append(ssInfo)

if args.verbose:
    print "Found these samplesources: "
    for eachSS in allSS:
        print " " + eachSS['title'] + " "

# Update limits to those provided
if args.limits:
    allLimits = args.limits.split(',')
    for ss in allSS:
        if ( len(allLimits) < len(allSS) ) and ( allSS.index(ss) == len(allLimits) ):
            print "%s's limit's defaulting to inf." % ss['title']
        else:
            ss['limit'] = allLimits[allSS.index(ss)]

else:
    print "Sample source limits will default to infinity."






# Handle question limits
qInfo = {}
allQs = []

if args.questions:
    theQuestions = args.questions

    # Go through the specified questions
    for eachQ in theQuestions:

        # Check if row limits specified
        if ":" in eachQ:
            eachQLabel  = eachQ.split(':')[0]
            eachQLimits = eachQ.split(':')[1]

            if len(eachQLimits.split(',')) >= 1 and eachQLimits != '':
                limitsSpecified = True
            else:
                limitsSpecified = False

        else:
            eachQLabel = eachQ
            limitsSpecified = False

        # Make sure question element exists in the survey
        qEl = checkOutEl(theSurvey, eachQLabel)
        if qEl == None:
            exit("Question %s not found!" % eachQLabel)
        originalTitle = ""
        # Prompt for quota table title if necessary
        if not args.skip and qEl.title != originalTitle:
            tableTitle = promptTitle(qEl.title)
            originalTitle = strip_tags(qEl.title)
        else:
            tableTitle = strip_tags(qEl.title)

        # The question element is found, if limits specified ensure there are that many rows in the question
        if limitsSpecified:
            if len(eachQLimits.split(',')) != len(qEl.rows):
                exit("You specified %s limits for %s but %s only has %s rows." % (len(eachQLimits.split(',')),qEl.label,qEl.label,len(qEl.rows)))


            
        
# Make sure the variable exists
#myExtras = checkExtra(variable)
