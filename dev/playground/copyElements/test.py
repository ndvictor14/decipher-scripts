#!/usr/bin/env hpython

import pkg_resources, hstub
from hermes import Survey, misc
from util import fatal
import argparse
import os
import sys
import subprocess
import xlwt
import re
from xml.dom import minidom
import xml.etree.ElementTree as ET
from HTMLParser import HTMLParser




surveyName = "."

surveyName = misc.expandSurveyPath(surveyName)
theSurvey = Survey.loadNoException(surveyName)
if theSurvey == None:
    exit("Unable to open specified survey: %s" % surveyName)



output = open('output.xml', 'w')
for eachEl in theSurvey.root.qElements:
  if eachEl.label == 'Q1':
    output.write(eachEl)
    
