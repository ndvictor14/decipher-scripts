#!/usr/bin/env hpython

################################################################
# Survey.py playground                                         #
################################################################
__author__ = 'Victor'

import pkg_resources, hstub
from hermes import Survey, misc
from util import fatal

import sys

pkg_resources, hstub

# Survey Object 
surveyName = sys.argv[1]
surveyName = misc.expandSurveyPath(surveyName)
theSurvey = Survey.loadNoException(surveyName)


# Survey States
if theSurvey.root.state.closed:
    exit("Survey state is closed. If you really want to make changes to this, set it to testing.")
elif theSurvey.root.state.live:
    exit("Survey is LIVE! I will not let you corrupt data. If you're in a temp set it to testing!")


# Grab samplesources
ms = theSurvey.root.samplesources.children

#Sample source titles
for eachSS in ms:
    print eachSS.title



# Grab Question elements
print theSurvey.root.qElements
for eachQ in theSurvey.root.qElements:
#    print eachQ.title # Print's each question's title :)
    for eachR in eachQ.rows:
        print eachR.cdata # This return these elements text

# print theSurvey.root.elements # all elements in the survey

