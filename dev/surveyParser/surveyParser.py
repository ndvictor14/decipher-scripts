#!/usr/bin/env hpython

###################################################################
# Helper functions for parsing a survey                           #
###################################################################
__author__ = 'Victor'

import pkg_resources, hstub
from hermes import Survey, misc
from util import fatal

import sys

pkg_resources, hstub

# Survey Object 
surveyName = sys.argv[1]
surveyName = misc.expandSurveyPath(surveyName)
theSurvey = Survey.loadNoException(surveyName)


# Return list of samplesource titles
def ssTitles(sv):
    results = []
    # Grab samplesources
    ms = theSurvey.root.samplesources.children
   #Sample source titles
   for eachSS in ms:
       results.append(eachSS.title)

