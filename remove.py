#!/usr/bin/python

import os

#open file
myFile = open("variables.dat", "r")

#get all the lines
lines = myFile.readlines()

#close file
myFile.close()

#ids to remove
removeID = ['7vxn0q7kvft6dt9z','1hvgs19bvqqbahu5','9ksffnw2ba1ucjeq','tkk7f3c39s902an0','jzqdcrf3hebkn2dz','mxywv5ypga0gwc0p','4srmx4nxevh6jewk','az5veccn0enffm2d','9vmfvbdbw3gnfbhk','aqgm4jgs48jbqpt4','auyygv321ub1krdh','ptr1kd3c664szbbm','q7vn0ftjzmbgm4da','szj7fduq7t85vsnt','tf4truf2pvhw8svk','qzerzgtkqdhfk0au','0vy14vvhm2gtudun','f50d56p2cq9v70gx','hn72rxtu3qdwk7t7','b91a7b7pjzm00mak','9e3s2ng06au4efw8','bgzu1e8x88pacep1','bbrwx40gnrzzs0gr','s9886p23usrva3m1','4xdyxz0tpntttzst','mrbkjg9wr9qct24y','n88p7gcdpxkcvn5n','rgqzduzugc0atpkp','ykncaekhha7vyqts','mcsuewxqwejk55au','4m3d72dwn9t5e3mg','r1s0et5mvedrge5b','urkvr1cxrbnxex2n','k5qx0uh3a8h81re0','5yt35t14yxh44kt9','ge8gax98d03j313h','871jarg7a6fcapbb','mtu1jvyp5ppvvmbc','r8zuyn179hyvpp7d','83f3y345arp8ajh8','hej3h6ed7zjwq47m','15e5a5f25s3uyu99','mz9wwrqwgv4d8zzg','98a63z265cg9e3zf','uk1kdqpxexw3xqye','a8au1g7kpcuy4jt6']

#open file for writing
myFile = open("variables.dat", "w")


#write lines to file if uuid not in remove
for line in lines:
  cols = line.split("\t")
  myUUID = cols[1]
  if myUUID not in removeID:
    myFile.write(line)
      
#close file
myFile.close()
