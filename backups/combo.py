#!/usr/bin/python
#Filename : combo.py

import re
import sys

combo_file = sys.stdin.readlines()

combo_num = int(re.sub('<!-- combo count: ([0-9]+) -->', r'\1', combo_file[2]))

p = re.compile("<row label='%s'>(.*?)<\/row>" % ",".join(["([0-9]+)" for x in range(combo_num)])) # <row label='([0-9]+),...'>(.*?)<\/row>

lines = [re.sub('\n', '', line) for line in combo_file]

sys.stdout.write("<!-- combo count: %s -->\n" % combo_num)

for line in lines[3:-1]:
    indexes_arr = re.sub(p, r'%s' % ",".join(["\%s" % x for x in range(combo_num+1)[1:]]), line).split(',') # \1,\2,...
    labels_arr = ['r%s' % (int(x)+1) for x in indexes_arr]

    desc = re.sub(p, r'\%s' % (combo_num+1), line)
    indexes = ",".join(indexes_arr)
    labels = ",".join(labels_arr)

    new_line = "  <row label='%s'> '%s' - %s</row>\n" % (indexes,labels,desc)
    sys.stdout.write(new_line)

sys.stdout.write("  %s\n" % lines[-1])
sys.stdout.flush()
