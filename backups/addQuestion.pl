#!/usr/bin/perl
# Author: Victor Hernandez (vhernandez@decipherinc.com)
# Date: June 26, 2014
# Description: This file is going to do MAGIC and make the turf EVERYTHING for you :)

use strict;
use Getopt::Long();

#sub routine for printing usage options
sub usage {
    my $message = $_[0];
    if (defined $message && length $message) {
      $message .= "\n"
	  unless $message =~ /\n$/;
    }

    my $command = $0;
    $command =~ s#^.*/##;

   print STDERR (
      $message,
      "usage: $command -q questionLabel [-v] [-oQ] [-oD] [-fQ turfQuestionRows.txt] [-fD turf-data.txt] [-fX spreadSheetFile.xls]\n" .
      "       ...\n" .
      "       ...\n" .
      "       ...\n"
       );

   die("\n")
}


my $help;
my $questionLabel;
my $verbose;
my $questionFile;
my $dataFile;
my $questionFileName;
my $dataFileName;
my $xlName;

Getopt::Long::GetOptions('h' => \$help, 'help' => \$help, 'v' => \$verbose, 'verbose' => \$verbose, 'q=s' => \$questionLabel, 'oQ' => \$questionFile, 'oD' => \$dataFile, 'fQ=s' => \$questionFileName, 'fD=s' => \$dataFileName, 'fX=s' => \$xlName);

if ($help){
    usage();
}

usage("Please specify the TURF question label with the -q option")
    unless defined($questionLabel);

if ($verbose){
    print "Thank you for using this AWESOME TURFulator!\n";
}

#Open the survey file and look for the question lable provided
if ($verbose){
    print "Opening survey.xml.\n";
}


my $file = "survey.xml"; 
open(XML, $file);
my @xml = <XML>;
close(XML);
my $offset = 0;

#open(FILE, "+<:encoding(UTF-8)", "survey.xml")
#    or die("Unable to open survey.xml in the current directory!\n");

if ($verbose){
    print "Searching for question with label $questionLabel...\n";
}

my $rowCount = 0;

#Add the new question for turf-analyze
foreach my $line (@xml){
    if ($line =~ m/label="\Q$questionLabel"/){
	if ($verbose){
	    print "Question Found!\n";
	    print "Generating New Question ".$questionLabel."_CB\n";
	}
	my @array = @xml;        # Copy
	while (my $token = shift @array) {
	    $rowCount++ if $token =~ m/.+<row.+/;
	    shift @array if $token !~ m/<suspend\/>/; # consumes the next element
	    $offset++;
	    if ($token =~ m/<suspend\/>/){
		$offset++;
		print $token." and ".$offset."\n";
		splice @xml, $offset, 0 ,  "<checkbox label=\"".$questionLabel."_CB\" atleast=\"1\" title=\"Checkbox $questionLabel\" onLoad=\"copy('$questionLabel', rows=True)\" virtual=\"setIfEqual($questionLabel,0)\" where=\"none,notdp\"/>\n\n";
		if ($verbose){
		    print "Question successfully added.\n";
		}
		print "Rows: $rowCount\n";
		last;   
	    }
	}
    }
    $offset++;
}

#Let dynamically get the number of rows in the question to run the turf-analyze script



open (OUT, ">", "trial.xml");
foreach my $line (@xml){
    print OUT $line;
}

