#!/usr/bin/perl
#Author: Victor Hernandez (vhernandez@decipherinc.com)
#Date: May 25,2014
#Description: Removes specified characters from files. The script can also delimit a file by a character if needed. (ie replace "|" with tabs or any other character).

use Getopt::Long ();


#sub routine for printing usage options
sub usage {
    my $message = $_[0];
    if (defined $message && length $message) {
      $message .= "\n"
	  unless $message =~ /\n$/;
    }

    my $command = $0;
    $command =~ s#^.*/##;

   print STDERR (
      $message,
      "usage: $command -f file1,file2,etc. [-d delimiter] [-r remove,remove,etc.]" .
      "       ...\n" .
      "       ...\n" .
      "       ...\n"
       );

   die("\n")
}

my $help;
my $data; #Files to clean up
my $d; #Delimeter to create tabs from (ie what character should be repleaced with tabs)
my $r;  #Additional things to remove (ie replace with blanks"
my $v; #verbose options
my $s2t; #Space to tab option

    Getopt::Long::GetOptions ('help' => \$help, "f=s" => \$data, "d=s" => \$d, "r=s" => \$r, 'v'=>\$v, 's' =>\$s2t)
      or usage("Invalid command line options. If you are unsure on how to use the script try the --help option.\n");

usage("Please specify file(s) using -f file1,file2,etc.\n")
    unless defined $data;

usage("Please specify a character to remove using -r character.\nIf you are trying to delimit, pass the delimeter as follows: -d delimiter.\nIf you want to replace multiple spaces with a single tab use the -s option.")
    unless (defined $r || defined $d || defined $s2t);

if ($v){
    print "Parsing file names...\n";
}

#split filenames and additional removes passed in
my @files = split(/,/, $data);
my @additionals;
if (defined $r){
  @additionals = split(/,/, $r);
}

if ($v){
    print "Files being cleaned: @files\n";
    if (scalar(@additionals) > 0){
	print "Additional characters being removed: @additionals\n";
    }
}

#Test to see that files specified exist
foreach my $file (@files){
    if (-e $file){
	if ($v){
	    print "$file found.\n";
	}
    }
    else{
	die("File $file not found in the directory!\nProcess has been killed.\n\n");
    }
}

#At this point all files should be checked for existence and we can get to the fun stuff :D
foreach my $file (@files){
    my $out = "clean".$file;
    #Warn user when overwriting file.
    if (-e $out){
	print "File named $out already exists!\nIf you continue, the file will be overwritten.\nWould you like to continue? Y|N : ";
	my $ans = <>;
	if ($ans =~ m/n|N|no|NO|nO|No|NO/){
	    die("\n\nProcess aborted!\n");
	}
	else{
	    print "File $file will be overwritten.\n";
	}
    }
    
    if ($v){
	print "Creating output file: $out for $file. \n";
	print "Running 'dos2unix' command.\n";
    }

    system('dos2unix', $file); #replace all those yucky meta characters -_-

    open(FILE, "<", $file);
    open(OUT, ">", $out);

    if ($v){
	print "Cleaning file: $file\n";
    }

    foreach my $line(<FILE>){
	if (defined $r){
	    foreach my $junk (@additionals){
		$line =~ s/$junk//g;
	    }
	}
	if(defined $d){
	    if ($v){
		print "Delimitting file: $file by $d.\n";
	    }
	    $line =~ s/\Q$d/\t/g;
	}
	if ($s2t){
	    if ($v){
		print "Space to tab option being applied to $file.\n";
	    }
	    $line =~ s/[ ]+/\t/g;
	}
	print OUT $line;
    }
    if ($v){
	print "Finished cleaning $file.\nClosing $file and $out.\n";
    }
    close(FILE);
    close(OUT);
}

if ($v){
    print "Successfully finished cleaning files.\n";
}
