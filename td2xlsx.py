import sys
import argparse
from xlsxwriter import Workbook
import sys

import csv, codecs, cStringIO

def run():    
    parser = argparse.ArgumentParser(description='Converts tab delimited files to xlsx files')
    parser.add_argument('input', help='Tab Delimited Input File or Files', nargs="*")
    parser.add_argument('output', help='XLSX Output File')
    parser.add_argument('-s','--strings-only', help='Convert everything to a string for the xlsx output', required=False, action='store_true')
    parser.add_argument('-b','--bold', help='Makes font bold', required=False, action='store_true' )
    parser.add_argument('-u','--underline', help='Makes font underlined', required=False, action='store_true' )
    parser.add_argument('-i','--italics', help='Makes font italicized', required=False, action='store_true' )
    parser.add_argument('-f','--font-color', help='Font color. Specify an html color code FF0000 or a basic color name e.g. "red"', required=False, nargs=1, metavar='color')
  
    args = vars(parser.parse_args())

    #print args

    #data = sys.stdin.readlines()
    #for line in sys.stdin:

    
    strings_only = False
    if args['strings_only'] == True:
        strings_only = True
       
    
    #Set formatting
    format = {'bold':False,'italic':False,'underline':False,'font_color':'black'}
       
    if args['bold'] ==True:
        format['bold'] = True
    if args['underline'] ==True:
        format['underline'] = True
    if args['italics'] ==True:
        format['italic'] = True
    
    #set color    
    if args['font_color'] is not None:
        format['font_color'] = args['font_color'][0]
       
       
    tsv_files = args['input']
    xlsx_file = str(args['output'])

    writeWorkBook(tsv_files,xlsx_file,strings_only,format)


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def writeWorkBook(tsv_files,xlsx_file,strings_only,format):
    # Create an XlsxWriter workbook object and add a worksheet.
    workbook = Workbook(xlsx_file)
       
    format = workbook.add_format(format)    
        
    #write each file to it's own sheet
    for tsv_file in tsv_files:        
        # Create a TSV file reader.
        #tsv_reader = csv.reader(open(tsv_file, 'rb'), delimiter='\t')
        tsv_reader = UnicodeReader(open(tsv_file.strip(), 'rb'), delimiter='\t')
    
        worksheet = workbook.add_worksheet(tsv_file.split('.')[0].strip())   
   
        # Read the row data from the TSV file and write it to the XLSX file.        
        for row, data in enumerate(tsv_reader):            
            for col,data1 in enumerate(data):
                if strings_only:
                    worksheet.write_string(row,col,data1,format)
                else:
                    if is_number(data1):
                        worksheet.write_number(row,col,float(data1),format)
                    else:
                        worksheet.write_string(row,col,data1,format)
                    
                
                #worksheet.write_row(row, 0, data)

    # Close the XLSX file.
    workbook.close()

    
#Below code used for utf-8 and taken from python documentation    
class UTF8Recoder:
    """
    Iterator that reads an encoded stream and reencodes the input to UTF-8
    """
    def __init__(self, f, encoding):
        self.reader = codecs.getreader(encoding)(f)

    def __iter__(self):
        return self

    def next(self):
        return self.reader.next().encode("utf-8")

class UnicodeReader:
    """
    A CSV reader which will iterate over lines in the CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        f = UTF8Recoder(f, encoding)
        self.reader = csv.reader(f, dialect=dialect, **kwds)

    def next(self):
        row = self.reader.next()
        return [unicode(s, "utf-8") for s in row]

    def __iter__(self):
        return self

class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

def unicode_csv_reader(unicode_csv_data, dialect=csv.excel, **kwargs):
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.reader(utf_8_encoder(unicode_csv_data),
                            dialect=dialect, **kwargs)
    for row in csv_reader:
        # decode UTF-8 back to Unicode, cell by cell:
        yield [unicode(cell, 'utf-8') for cell in row]

def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        yield line.encode('utf-8')    
    
if __name__ == '__main__': sys.exit(run())
