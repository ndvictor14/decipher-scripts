#!/usr/bin/python2.7

import subprocess
from subprocess import PIPE,Popen

# Test promp answering via python


workingDir = subprocess.check_output(['pwd'])


prompt = Popen(['./myprompt.py'], cwd=workingDir.strip('\n'), stdin=PIPE)
prompt.stdin.write('yes') # Answer prompt
prompt.stdin.flush()
