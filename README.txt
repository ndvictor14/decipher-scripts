This directory contains my custom scripts along along with anything that I am currently working on.

The dev directory contains scripts that are still under development and will require sufficient amount of time to be set aside to complete.

The test directory contains scripts that can be considered in "beta". These can be used but may have some limitations. 

The backup directory was created to contain a backup of the scripts in the event that something were ever deleted/overwritten.
