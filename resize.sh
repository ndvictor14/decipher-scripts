#!/bin/sh

for file in *.jpg
do
    echo "Resizing file $file ..."
    convert "$file" -resize 100x100\> -size 195x260 xc:white +swap -gravity center -composite "$file"
done
