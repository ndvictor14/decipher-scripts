#!/usr/bin/env python
"""
Search v2/hermes by string

This was modified by Jeremy from Ryan Scarberry's "findfatal.py"

"""
from twisted.python import usage
from subprocess import Popen, PIPE
from glob import glob
import re
import sys
import os


EDITOR = 'vim'
HERMES_DIR = '/home/jaminb/v2/hermes'


class ListExceptionsException(Exception):
    pass


def remove_blanks(lines):
    return [line for line in lines if line.strip()]


def list_exceptions(phrase=None):
    filesFound = []
    if phrase is not None:
        p1 = Popen(['grep', '-rlsn', phrase, HERMES_DIR], stdout=PIPE)
        filesFound = remove_blanks(p1.communicate()[0].split('\n'))
    return filesFound


def open_files(files):
    if not files:
        return None

    if len(files) == 1:
        openPath = files[0]
    else:
        fileDict = dict((i + 1, path) for i, path in enumerate(sorted(files)))

        print "\nFound the following matches:\n"

        for i, path in sorted(fileDict.items()):
            fileName = os.path.normpath(path)
            print "[{index}]: {path}".format(index=i, path=fileName)
        print

        try:
            while True:
                index = raw_input("Open Fileno: ")
                try:
                    openPath = fileDict[int(index)]
                except (KeyError, ValueError):
                    print "Invalid Fileno: %s" % index
                else:
                    break

        except KeyboardInterrupt:
            print
            return None

    Popen([EDITOR, '-R', openPath ], env=os.environ.copy()).wait()


class Options(usage.Options):

    optParameters = [['phrase', 'p', None, 'String to search for']
                    ]

    def parseArgs(self, *args):
        if len(sys.argv) == 1:
            raise usage.UsageError

        if self['phrase'] is None and args:
            self['phrase'] = ' '.join(args)


def main():
    config = Options()
    try:
        config.parseOptions()
    except usage.UsageError, e:
        print >> sys.stderr, e
        print >> sys.stderr, config
        sys.exit(1)
    try:
        files = list_exceptions(config['phrase'])
        open_files(files)
    except ListExceptionsException, e:
        print >> sys.stderr, e
        print >> sys.stderr, config
        sys.exit(1)
    return 0


if __name__ == '__main__':
    sys.exit(main())
