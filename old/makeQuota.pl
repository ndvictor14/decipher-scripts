#!/usr/bin/perl
# This file will look at the survey file and generate a simple quota.xls file based on the list values
# Author: Victor Hernandez
# Date: August 9, 2014

use lib "/home/vhernandez/perl5/lib/perl5";
use lib "/home/vhernandez/perl5";
use lib '--install_base "/home/vhernandez/perl5"';
use lib "/home/vhernandez/perl5";


use strict;
exec(eval `perl -I ~/perl5/lib/perl5 -Mlocal::lib`);
use Spreadsheet::WriteExcel;

use Getopt::Long();
use Scalar::Util qw(looks_like_number);#sub routine for printing usage options

sub usage {
    my $message = $_[0];
    if (defined $message && length $message) {
      $message .= "\n"
	  unless $message =~ /\n$/;
    }

    my $command = $0;
    $command =~ s#^.*/##;

   print STDERR (
      $message,
      "usage: $command [-v] [-h] [-l QuotaLimit]\n" .
      "       ...\n" .
      "       ...\n" .
      "       ...\n"
       );

   die("\n")
}

# Option Variables
my $help;
my $verbose;
my @limit;

#Handle Options
Getopt::Long::GetOptions('h' => \$help, 'v' => \$verbose, 'l=s' => \@limit);


if ($help){
    exec('man /home/jaminb/v2/temp/victor/scripts/manpages/./makeQuota');
}

unless (-e "survey.xml"){
    print "A survey.xml file does not seem to exist in this directory!\n\n";
    exit();
}

@limit = split(/,/,join(',',@limit));

foreach my $limits (@limit){
    if (!looks_like_number($limits)){
	usage("Invalid limits passed");
    }
}

if (!@limit){
    @limit = "inf";
}

# Create quota sheet workbook
if (-e "quota.xls"){
    print "quota.xls file found!\nWould you like to continue and overwrite the file? (y|n)";
    my $answer = <>;
    if ($answer =~ m/n|N|no|NO/){
	print "Script Successfully Aborted!\n\nThanks for flying Vic :D\n";
	exit();
    }
}

if ($verbose){
    print "Creating quota.xls workbook...\n";
}

my $workbook  = Spreadsheet::WriteExcel->new('quota.xls');
my $defineSheet = $workbook->add_worksheet("Defines");
my $quotaSheet = $workbook->add_worksheet("Quotas");

if ($verbose){
    print "Workbook successfully created...\n";
    print "Opening survey file...\n";
}

my $file = "survey.xml";
open(XML, $file);
my @survey = <XML>;
my @copy = @survey;
close (XML);
my @listValues;
my @titles; # used for the descriptions 
my $temp = 0;

# Read through the array to find the different list values
if ($verbose){
    print "Determining list values and titles...\n";
}
while (my $line = shift @survey){
    if ($line =~ m/list="/){
	my $copy = $line;
	$copy =~ s/.*list=\"//;
	$copy =~ s/".*//;
	push(@listValues,$copy);
	while ($line !~ m/\/samplesource>/){
	    $line = shift @survey;
	    if ($line =~ m/title/){
		my $title = $line;
		if ($title =~ m/<title>/){
		    $title =~ s/.*<title>//;
		    $title =~ s/<\/title>.*//;
		}
		else{
		    $title =~ s/.*title=\"//;
		    $title =~ s/\".*//;
		}
		push(@titles,$title);
	    }
	}

    }
    last if $line =~ m/\/samplesources>/;
}

if ($verbose){
    for (my $x = 0 ; $x < scalar(@listValues) ; $x++){
	print "Adding list value ".$listValues[$x]." with title ".$titles[$x]."\n";
    }
}


if (scalar(@limit) == 0){
    @limit = "inf";
}
elsif (scalar(@limit) == 1){
    for (my $i = 0 ; $i < scalar(@listValues); $i++){
	$limit[$i] = @limit[0];
    }
}

if (scalar(@limit) != scalar(@listValues)){
    die("You seemed to have either given more or less limits than there are list values. Please check your arguments.");
}

foreach my $value (@listValues){
    chomp($value);
    $defineSheet->write($temp, 0, "list".$value);
    $defineSheet->write($temp, 1, "list=='".$value."'");
    $defineSheet->write($temp,2,@titles[$temp]);
    $temp++;
    $quotaSheet->write($temp,0,"list".$value);
    $quotaSheet->write($temp,1,@limit[$temp-1]);
    if ($verbose){
	print "List $value ($titles[$temp-1]) has a limit of $limit[$temp-1].\n";
    }
}

$quotaSheet->write(0,0,"#=Respondents By List");

if ($verbose){
    print "quota.xls file successfully generated!\n";
    print "Thanks for flying Vic :D\n";
}
