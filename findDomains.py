#!/usr/bin/python

counts = {}
with open('sentEmails.txt', 'r') as f:
    for l in f:
        cols = l.split('\t')
        dom = cols[0].split('.')[-1]
        if dom in counts:
            counts[dom] += 1
        else:
            counts[dom] = 1

with open('counts.txt', 'w') as o:
  o.write('TLD (.com)\tCount of ok Sends\n')
  for key, value in counts.iteritems():
    o.write(key + "\t" + str(value) + "\n")
            
